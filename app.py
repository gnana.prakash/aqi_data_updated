from flask import Flask
from flask_cors import CORS

from scripts.constants import app_configuration
from scripts.services.aqi_service import aqi_service


# declaring app
app = Flask(__name__)

# connecting all services
app.register_blueprint(aqi_service)

CORS(app, resources={r"/*": {"origins": "*"}})


@app.route('/')
def route():
    return "------------------ UI Services ---- version: v1.0. ------------------"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(app_configuration.PORT), debug=False, threaded=True, use_reloader=False)
