"""
Exceedance Parser
"""
# -----------------Start of Import statements------------------------ #
import json
from elasticsearch import Elasticsearch
from scripts.logging.glens_logging import logger as log

# -----------------end of Import statements------------------------ #
__ES_URL__ = ""
__ES_OBJ__ = None
data = []


def _decode_list(param_data):
    """
            This function is to _decode_list
            :param param_data:
            :return rv:
    """
    rv = param_data
    return rv


def _decode_dict(param_data):
    """
          This function is to _decode_dict
          :param param_data:
          :return rv:
    """
    rv =param_data
    return rv


def query_elastic_search_by_body(index_name, index_type, body_query, size=1000000):
    """
        This Function is to query_elastic_search_by_body
        :param index_name:
        :param index_type:
        :param body_query:
        :param size:
        :return:
    """
    try:
        es_response = __ES_OBJ__.search(index=index_name, doc_type=index_type, body=body_query, size=size)
    except Exception as e:
        log.exception("Index not found in Elastic Search. " + "Index -->" + index_name + " index_type-->" +
                         index_type + str(e))
        log.exception("Exception occurred in queryElasticSearchByBody() : ")
        es_response = {}
    json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
    return json_es_response_object


def fetch_records_from_es_json(json_obj):
    """
        This function is to fetch_records_from_es_json
        :param json_obj:
        :return:
    """
    record_list = []
    try:
        temp_json_obj = json_obj["hits"]["hits"]
        for record in temp_json_obj:
            record_list.append(record["_source"])
        return record_list
    except Exception as e:
        log.exception("Exception occured in queryElasticSearch" + str(e))
        return record_list


def query_elastic_search(index_name, index_type):
    """
        This function is to query_elastic_search
        :param index_name:
        :param index_type:
        :return:
    """
    json_es_response_object = {}
    try:
        es_response = __ES_OBJ__.search(index=index_name, doc_type=index_type, size=1000000)
        json_es_response_object = json.loads(json.dumps(es_response), object_hook=_decode_dict)
        return json_es_response_object
    except Exception as e:
        log.exception("Exception occured in queryElasticSearch" + str(e))
        return json_es_response_object


def get_details():
    """
        This Function is to get_details
        :return:
    """
    index_list = __ES_OBJ__.indices.get_aliases().keys()
    counter = 0
    for eachindex in index_list:
        if str(eachindex)[:4] == "site":
            counter += 1


def all_exceedance(from_date, to_date):
    """
        This function is to get all_exceedance
        :param from_date:
        :param to_date:
        :return:
    """
    data_var = []
    lowest_reading = []
    highest_reading = []
    hit_counter = []
    response = ""
    comment = ""
    serial = 1
    tempbody_content = query_elastic_search("tnpcb", "site")
    site_list_json = fetch_records_from_es_json(tempbody_content)
    for site in site_list_json:
        site_id = site["siteInfo"]["siteId"]
        site_name = ""
        parameter_name = ""
        temp_list = []
        body_query = {"from": 0, "size": 100000,
                      "query": {"range": {"timestamp": {"gte": str(from_date), "lte": str(to_date)}}}}
        response = query_elastic_search_by_body("wf_" + site_id, "event", body_query)
        if response is None or response == {} or response == []:
            continue
        for hit in response["hits"]["hits"]:
            if "TYPE" in hit["_source"]["comments"]:
                site_name = hit["_source"]["siteName"]
                parameter_name = hit["_source"]["parameterName"]
                if not temp_list.__contains__(site_name + "," + parameter_name):
                    temp_list.append(site_name + "," + parameter_name)
        greatest = 0
        lowest = 0
        flag = 0
        for obj in temp_list:
            counter = 0
            sdetail = str(obj).split(",")
            for hit in response["hits"]["hits"]:
                if "TYPE" in hit["_source"]["comments"] and sdetail[1] in hit["_source"]["parameterName"]:
                    comment = str(hit["_source"]["comments"]).replace(",", " ").split(" ")
                    if flag == 0:
                        greatest = float(comment[9])
                        lowest = float(comment[9])
                        flag = 1
                    if greatest < float(comment[9]):
                        greatest = float(comment[9])
                    if lowest > float(comment[9]):
                        lowest = float(comment[9])
                    counter += 1
            if flag == 1:
                lowest_reading.append(str(lowest))
                highest_reading.append(str(greatest))
                flag = 0
                hit_counter.append(str(counter))
                data_var.append([site_name, hit["_source"]["city"], sdetail[1], comment[4], comment[13], counter,
                                 str(lowest) + "-" + str(greatest), comment[14][:-1]])
                serial += 1
    return data_var


def site_exceedance(site_id, from_date, to_date):
    """
        This function is to site_exceedance
        :param site_id:
        :param from_date:
        :param to_date:
        :return:
    """
    try:
        data_var = []
        lowest_reading = []
        highest_reading = []
        hit_counter = []
        response = ""
        comment = ""
        serial = 1
        each_index = "wf_" + site_id
        site_name = ""
        temp_list = []
        body_query = {"from": 0, "size": 100000,
                      "query": {"range": {"timestamp": {"gte": str(from_date), "lte": str(to_date)}}}}
        response = query_elastic_search_by_body(each_index, "event", body_query)
        for hit in response["hits"]["hits"]:
            if "TYPE" in hit["_source"]["comments"]:
                site_name = hit["_source"]["siteName"]
                parameter_name = hit["_source"]["parameterName"]
                if not temp_list.__contains__(site_name + "," + parameter_name):
                    temp_list.append(site_name + "," + parameter_name)
        greatest = 0
        lowest = 0
        flag = 0
        for obj in temp_list:
            counter = 0
            sdetail = str(obj).split(",")
            for hit in response["hits"]["hits"]:
                if "TYPE" in hit["_source"]["comments"] and sdetail[1] in hit["_source"]["parameterName"]:
                    comment = str(hit["_source"]["comments"]).replace(",", " ").split(" ")
                    if flag == 0:
                        greatest = float(comment[9])
                        lowest = float(comment[9])
                        flag = 1
                    if greatest < float(comment[9]):
                        greatest = float(comment[9])
                    if lowest > float(comment[9]):
                        lowest = float(comment[9])
                    counter += 1
            if flag == 1:
                lowest_reading.append(str(lowest))
                highest_reading.append(str(greatest))
                flag = 0
                hit_counter.append(str(counter))
                data_var.append([site_name, hit["_source"]["city"], sdetail[1], comment[4], comment[13], counter,
                                 str(lowest) + "-" + str(greatest), comment[14][:-1]])
                serial += 1
        return data_var
    except Exception as e:
        log.exception("Exception in siteExceedance" + str(e))


def get_site_details(site_id):
    """
        This function is to get_site_details
        :param site_id:
        :return:
    """
    global __ES_URL__
    e_s_query = {"_source": "siteInfo"}
    tempbody_content = query_elastic_search_by_body("tnpcb", "site", e_s_query)
    site_list_json = fetch_records_from_es_json(tempbody_content)
    final_site_list = {}
    for site in site_list_json:
        try:
            if "siteInfo" in site:
                site_info = site["siteInfo"]
                if site_id == site_info["siteId"]:
                    final_site_list[site_id] = site_info["siteName"] + " - " + site_info["city"]
                    break
        except Exception as e:
            log.exception("Exception in getSiteDetails" + str(e))
    return final_site_list


def initialize_es(es_url):
    """
        This function is to initialize_es
        :param es_url:
        :return:
    """
    global __ES_URL__, __ES_OBJ__
    __ES_URL__ = es_url
    __ES_OBJ__ = Elasticsearch(es_url)
