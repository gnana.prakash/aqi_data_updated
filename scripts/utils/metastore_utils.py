import traceback
from datetime import datetime

from scripts.constants.app_configuration import ES_URL, USER_BASED_FILTER
from scripts.constants.app_configuration import SITE_APPROVAL
from scripts.logging.glens_logging import logger
from scripts.utils.esutility import ESUtility

es_obj = ESUtility(ES_URL)


def merge_site_hierarchy(user_id, user_type, camera=None, source_site_id=None):
    if source_site_id is None or source_site_id == "":
        site_list = es_obj.get_site_user_mapping(user_id, user_type)
        site_temp_json = es_obj.query_elastic_search("tnpcb", "site")
        site_json = es_obj.fetch_records_from_es_json(site_temp_json)
    else:
        site_list = [source_site_id]
        site_json = [es_obj.query_elastic_search_by_id("tnpcb", "site", source_site_id)]
    final_site_details_json = dict()
    final_site_details_json["siteId"] = list()
    site_id_list = list()
    if USER_BASED_FILTER == "True":
        user_details = es_obj.query_elastic_search_by_id("tnpcb", "userSetup", user_id)
        if "tags" in user_details and user_details["tags"] != "":
            pass
            # user_tags = user_details["tags"].split(",")
    for site in site_json:
        try:
            site_id = site["siteInfo"]["siteId"]
            if site_id not in site_list:
                continue
            if ("analyserSetup" not in site or
                    site["analyserSetup"] is None or
                    site["analyserSetup"] == {}):
                continue
            if ("acquisitionSetup" not in site or
                    site["acquisitionSetup"] is None or
                    site["acquisitionSetup"] == {} or
                    "monitoringDetails" not in site["acquisitionSetup"] or
                    site["acquisitionSetup"]["monitoringDetails"] is None or
                    site["acquisitionSetup"]["monitoringDetails"] == {}
            ):
                continue
            # print site["acquisitionSetup"]
            monitoring_details = site["acquisitionSetup"]["monitoringDetails"]
            temp_site_detail = dict()
            temp_site_detail["key"] = site["siteInfo"]["siteId"]
            site_id = site["siteInfo"]["siteId"]
            temp_site_detail["value"] = site["siteInfo"]["siteName"] + "," + site["siteInfo"]["city"]
            monitoring_type_json = dict()
            for key in site["analyserSetup"]:
                if (key not in ["Ambient", "Effluent", "Emission"] or
                        site["analyserSetup"][key] is None or
                        site["analyserSetup"][key] == []):
                    continue
                if camera is not None and key not in ["Effluent", "Emission"]:
                    continue
                monitoring_id_list = []
                monitoring_label_list = []
                monitoring_unit_list = {}
                for monitoring_unit in site["analyserSetup"][key]:
                    monitoring_id = monitoring_unit["monitoringId"]
                    monitoring_label = get_monitoring_label_mapping(monitoring_details, monitoring_id, key)
                    analyzer_id = monitoring_unit["analyser"]["key"]
                    parameter_name = monitoring_unit["parameter"]["value"]
                    # if camera is not None and key == "Effluent" and parameterName != "Camera":
                    if camera is not None and key in ["Effluent", "Emission"] and parameter_name != "Camera":
                        continue
                    # if camera is None and key == "Effluent" and parameterName == "Camera":
                    if camera is None and key in ["Effluent", "Emission"] and parameter_name == "Camera":
                        continue
                    if monitoring_id not in monitoring_unit_list:
                        monitoring_unit_list[monitoring_id] = {}
                        monitoring_unit_list[monitoring_id]["analyzerId"] = []
                        monitoring_id_list.append(monitoring_id)
                        monitoring_label_list.append(monitoring_label)
                    if analyzer_id not in monitoring_unit_list[monitoring_id]["analyzerId"]:
                        monitoring_unit_list[monitoring_id]["analyzerId"].append(analyzer_id)
                        temp_monitoring_unit = dict()
                        # monitoringLabel = getMonitoringLabelMapping(monitoringDetails, monitoringId, key)
                        temp_monitoring_unit["monitoringId"] = monitoring_id
                        temp_monitoring_unit["monitoringLabel"] = monitoring_label
                        temp_monitoring_unit["analyser"] = monitoring_unit["analyser"]
                        temp_monitoring_unit["parameter"] = []
                        temp_monitoring_unit["parameter"].append(monitoring_unit["parameter"])
                        monitoring_unit_list[monitoring_id][analyzer_id] = temp_monitoring_unit
                    else:
                        monitoring_unit_list[monitoring_id][analyzer_id]["parameter"].append(
                            monitoring_unit["parameter"])
                if monitoring_unit_list != {} and monitoring_id_list != []:
                    monitoring_type_json[key] = monitoring_unit_list
                    monitoring_type_json[key]["monitoringId"] = monitoring_id_list
                    monitoring_type_json[key]["monitoringLabel"] = monitoring_label_list
            if monitoring_type_json != {}:
                site_id_list.append(site_id)
                final_site_details_json[site_id] = monitoring_type_json
                final_site_details_json["siteId"].append(temp_site_detail)
        except Exception as e:
            logger.exception("Exception in mergeSiteHirearchy " + str(e))
    return final_site_details_json


def get_monitoring_label_mapping(monitoring_list, monitoring_id, monitoring_type):
    monitoring_label = ""
    try:
        for monitoring_unit in monitoring_list:
            if monitoring_id == monitoring_unit["monitoringId"] and \
                    monitoring_type == monitoring_unit["monitoringType"]:
                return monitoring_unit["monitoringLabel"]
    except Exception as e:
        logger.exception("Error in getMonitoringLabelMapping " + str(e))
    return monitoring_label


def get_units_list():
    temp_body_content = es_obj.query_elastic_search("tnpcb", "unit")
    units_list = es_obj.fetch_records_from_es_json(temp_body_content)
    final_units_list = []
    for unit in units_list:
        final_units_list.append(unit["unitValue"])
    return final_units_list


def get_event_details():
    temp_body_content = es_obj.query_elastic_search("tnpcb", "event")
    event_records = es_obj.fetch_records_from_es_json(temp_body_content)
    event_list = []
    for event_data in event_records:
        event = dict()
        event["key"] = event_data["eventId"]
        event["value"] = event_data["eventMessage"]
        event_list.append(event)
    return event_list


def get_calibrator_details():
    final_calibrator_list = []
    tempbody_content = es_obj.query_elastic_search("tnpcb", "calibrator")
    body_content = es_obj.fetch_records_from_es_json(tempbody_content)
    for calibrator in body_content:
        temp_calibrator = {}
        temp_calibrator["key"] = calibrator["calibratorId"]
        temp_calibrator["value"] = calibrator["calibratorName"]
        final_calibrator_list.append(temp_calibrator)
    return final_calibrator_list


def add_remote_cal_trigger(siteid, remote_calid, remote_cal_json):
    # Logic for new Remote Calibration
    remote_id = unique_id_generator("remotecallog")
    remote_cal_json["remotecallogId"] = remote_id
    remote_cal_json["trigger_time"] = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    es_obj.update_elastic_search("tnpcb", "remotecallog", remote_id, remote_cal_json)


def unique_id_generator(field):
    field_json = es_obj.query_elastic_search_by_id("tnpcbconfig", "uniqueid", field)
    if field_json == {}:
        field_json = {"uniqueId": 1}
        uid = 1
        es_obj.update_elastic_search("tnpcbconfig", "uniqueid", field, field_json)
    else:
        uid = field_json["uniqueId"]
        uid = uid + 1
        field_json = {"uniqueId": uid}
        es_obj.update_elastic_search("tnpcbconfig", "uniqueid", field, field_json)
    return field + "_" + str(uid)


def check_time_is_greater(date_object1, date_object2):
    try:
        if date_object1 >= date_object2:
            return True
        else:
            return False
    except Exception as e:
        logger.exception("Exception caught when comparing the time -> " + str(e))
        logger.exception(traceback.format_exc())


def parameters_builder(site_id, is_approval_site="False"):
    site_final_json = {}
    parameter_map = {}
    if SITE_APPROVAL == "True" and is_approval_site == "True":
        site_json = es_obj.query_elastic_search_by_id("tnpcb", "temp_site", site_id)
    else:
        site_json = es_obj.query_elastic_search_by_id("tnpcb", "site", site_id)
    site_name = site_json["siteInfo"]["siteName"]
    site_label = site_json["siteInfo"]["siteLabel"]
    city = site_json["siteInfo"]["city"]
    industry = site_json["siteInfo"]["industry"]
    latitude = site_json["siteInfo"]["latitude"]
    longitude = site_json["siteInfo"]["longitude"]
    site_ip = ""
    if "acquisitionSetup" in site_json and "ipAddress" in site_json["acquisitionSetup"]:
        site_ip = site_json["acquisitionSetup"]["ipAddress"]
    site_status_json = es_obj.query_elastic_search_by_id("site_status", "alarms", site_id)
    last_synchronized = ""
    site_config_last_updated_time = ""
    last_calibrated_on = ""
    site_status = ""
    if "lastSynchronized" in site_status_json and site_status_json["lastSynchronized"] != "":
        last_synchronized = site_status_json["lastSynchronized"]
    if "siteConfigLastUpdatedTime" in site_status_json and site_status_json["siteConfigLastUpdatedTime"] != "":
        site_config_last_updated_time = site_status_json["siteConfigLastUpdatedTime"]
    if "lastCalibratedOn" in site_status_json and site_status_json["lastCalibratedOn"] != "":
        last_calibrated_on = site_status_json["lastCalibratedOn"]
    else:
        last_calibrated_on = site_config_last_updated_time
    if "SiteStatus" in site_status_json and site_status_json["SiteStatus"] != "":
        site_status = site_status_json["SiteStatus"]

    site_current_status = site_status_json["Message"]
    site_details = dict()
    site_details["siteId"] = site_id
    site_details["siteName"] = site_name
    site_details["siteLabel"] = site_label
    site_details["city"] = city
    site_details["industry"] = industry
    site_details["siteIp"] = site_ip
    site_details["lastSynchronized"] = last_synchronized
    site_details["SiteStatus"] = site_status
    site_details["latitude"] = latitude
    site_details["longitude"] = longitude
    site_details["siteConfigLastUpdatedTime"] = site_config_last_updated_time
    site_details["siteCurrentStatus"] = site_current_status
    site_details["lastCalibratedOn"] = last_calibrated_on
    site_details["AcquisitionSystem"] = site_json["acquisitionSetup"]["acquisitionSystemModel"]
    site_details["AcquisitionVersion"] = site_json["acquisitionSetup"]["acquisitionSystemVersion"]
    monitoring_type_json = site_json["sensorSetup"]["subvalues"]
    monitoring_list = site_json["acquisitionSetup"]["monitoringDetails"]
    for monitoring_type in monitoring_type_json:
        for monitoring_unit in monitoring_type_json[monitoring_type]:
            monitoring_id = monitoring_unit["monitoringId"]
            for analyser in monitoring_unit["analyser"]:
                analyzer_id = analyser["key"]
                analyzer_name = analyser["value"]
                for parameter in analyser["parameterDetails"]:
                    parameter_id = parameter["key"]
                    key = monitoring_type + "." + monitoring_id + "." + analyzer_id + "." + parameter_id
                    final_parameter = dict()
                    final_parameter["siteId"] = site_id
                    final_parameter["siteName"] = site_name
                    final_parameter["city"] = city
                    final_parameter["industry"] = industry
                    final_parameter["monitoringType"] = monitoring_type
                    final_parameter["monitoringId"] = monitoring_id
                    final_parameter["analyzerId"] = analyzer_id
                    final_parameter["analyzerName"] = analyzer_name
                    final_parameter["parameterId"] = parameter_id
                    final_parameter["GaugeMinimum"] = parameter["deviceRangeLowerLimit"]
                    final_parameter["GaugeMaximum"] = parameter["deviceRangeUpperLimit"]
                    final_parameter["aCoefficient"] = parameter["aCoefficient"]
                    final_parameter["bCoefficient"] = parameter["bCoefficient"]
                    final_parameter["compPort"] = parameter["compPort"]
                    final_parameter["ChannelNo"] = parameter["channelNo"]
                    final_parameter["alarmsMaxThreshold"] = parameter["alarmsMaxThreshold"]["rangeValue"]
                    final_parameter["alarmsMaxThresholdId"] = parameter["alarmsMaxThreshold"]["alarmModel"]["key"]
                    final_parameter["alarmsMaximum"] = parameter["alarmsMaximum"]["rangeValue"]
                    final_parameter["alarmsMinimum"] = parameter["alarmsMinimum"]["rangeValue"]
                    final_parameter["alarmsMinThreshold"] = parameter["alarmsMinThreshold"]["rangeValue"]
                    final_parameter["unit"] = parameter["units"]
                    final_parameter["representativenessRate"] = parameter["aggregationCalculationsRepRate"]
                    final_parameter["parameterName"] = parameter["value"]
                    final_parameter["cameraUrl"] = parameter["cameraUrl"]
                    try:
                        final_parameter["cameraDetails"] = parameter["cameraDetails"]
                    except Exception as e:
                        logger.error("Error in parameter mapping -- being passed " + str(e))
                        pass
                    final_parameter["monitoringLabel"] = get_monitoring_label_mapping(monitoring_list, monitoring_id,
                                                                                      monitoring_type)
                    final_parameter["lastUpdated"] = "2014-01-01T00:00:00Z"
                    final_parameter["lastValidated"] = "2016-01-01T00:00:00Z"
                    final_parameter["lastValidationSaved"] = "2016-01-01T00:00:00Z"
                    try:
                        final_parameter["lastUpdated"] = site_status_json[key + ".lastUpdated"]
                    except Exception as e:
                        logger.error("Error in parameter mapping -- being passed " + str(e))
                        pass
                    try:
                        final_parameter["lastValidated"] = site_status_json[key + ".lastValidated"]
                    except Exception as e:
                        logger.error("Error in parameter mapping -- being passed " + str(e))
                        pass
                    try:
                        final_parameter["lastValidationSaved"] = site_status_json[key + ".lastValidationSaved"]
                    except Exception as e:
                        logger.error("Error in parameter mapping -- being passed " + str(e))
                        pass

                    last_validation_saved_obj = datetime.strptime(final_parameter["lastValidationSaved"],
                                                                  '%Y-%m-%dT%H:%M:%SZ')
                    last_validated_obj = datetime.strptime(final_parameter["lastValidated"], '%Y-%m-%dT%H:%M:%SZ')
                    if check_time_is_greater(last_validated_obj, last_validation_saved_obj):
                        final_parameter["lastValidationSaved"] = final_parameter["lastValidated"]

                    final_parameter["key"] = key
                    final_parameter["enableParameter"] = "True"
                    if ("enableParameter" in parameter and parameter["enableParameter"] is not None and
                            str(parameter["enableParameter"]).strip() != ""):
                        final_parameter["enableParameter"] = parameter["enableParameter"]
                    parameter_map[key] = final_parameter
    site_final_json["siteDetails"] = site_details
    site_final_json["parameterMap"] = parameter_map
    site_final_json["siteJson"] = site_json
    return site_final_json


def site_info_metadata_update(header_key):
    # print headerKey
    try:
        site_info_json = es_obj.query_elastic_search_by_id("tnpcbconfig", "site", "siteinfo")
        district_list = []
        if header_key in ["district", "roName", "tehsil"]:
            temp_district_content = es_obj.query_elastic_search("tnpcb", "district")
            district_list = es_obj.fetch_records_from_es_json(temp_district_content)
        for header in site_info_json["headerContent"]:
            if header["key"] == header_key:
                if header_key == "industry":
                    temp_ind_content = es_obj.query_elastic_search("tnpcb", "industryCat")
                    industry_cat_list = es_obj.fetch_records_from_es_json(temp_ind_content)
                    options = []
                    for industry_cat in industry_cat_list:
                        options.append(industry_cat["industryCategory"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                    break
                if header_key == "vendor":
                    options = []
                    temp_vendor_content = es_obj.query_elastic_search("tnpcb", "vendor")
                    vendor_list = es_obj.fetch_records_from_es_json(temp_vendor_content)
                    for vendor in vendor_list:
                        options.append(vendor["vendorName"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                    break
                if header_key == "roName":
                    options = []
                    for district in district_list:
                        options.append(district["regionalOffice"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                if header_key == "tehsil":
                    options = []
                    for district_obj in district_list:
                        if "tehsil" in district_obj:
                            for tehsil in district_obj["tehsil"].split(","):
                                if tehsil is not None and str(tehsil).strip() != "" and str(
                                        tehsil).strip() not in options:
                                    options.append(str(tehsil).strip())
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                if header_key == "district":
                    options = []
                    for district_obj in district_list:
                        if "district" in district_obj:
                            for district in district_obj["district"].split(","):
                                if district is not None and str(district).strip() != "" and str(
                                        district).strip() not in options:
                                    options.append(str(district).strip())
                    if options is not None and options != []:
                        header["options"] = sorted(options)
        # print siteInfoJson
        es_obj.update_elastic_search("tnpcbconfig", "site", "siteinfo", site_info_json)
    except:
        logger.exception("Exception in siteInfoMetadataUpdate")


def get_user_details(user_id, user_type, site_id, load_type):
    user_list = []
    if user_type == "Client":
        user_records = es_obj.fetch_site_to_regulator_mapping(site_id)
        user_data = es_obj.query_elastic_search_by_id("tnpcb", "userSetup", user_id)
        user = dict()
        user["key"] = user_data["userId"]
        user["value"] = user_data["userName"]
        user["userType"] = user_data["userRole"]
        for userRecord in user_records:
            user = dict()
            user["key"] = userRecord["userId"]
            user["value"] = userRecord["userName"]
            user["userType"] = userRecord["userRole"]
            user_list.append(user)
        return user_list
    temp_body_content = es_obj.query_elastic_search("tnpcb", "userSetup")
    user_records = es_obj.fetch_records_from_es_json(temp_body_content)
    for user_data in user_records:
        user = dict()
        user["key"] = user_data["userId"]
        user["value"] = user_data["userName"]
        user["userType"] = user_data["userRole"]
        if user_type == "Admin":
            user_list.append(user)
        elif (user_type == "Regulator" or
              user_type == "SuperRegulator" or
              user_type == "SuperRegulatorAdmin" or
              user_type == "Vendor" or
              user_type == "Nodal") and \
                load_type == "load":
            user_list.append(user)
        elif (user_type == "Regulator" or
              user_type == "SuperRegulator" or
              user_type == "SuperRegulatorAdmin" or
              user_type == "Vendor" or
              user_type == "Nodal") and \
                user_data["userRole"] != "Client":
            user_list.append(user)
        elif (user_type == "Regulator" or
              user_type == "SuperRegulator" or
              user_type == "SuperRegulatorAdmin" or
              user_type == "Vendor" or
              user_type == "Nodal") and \
                user_data["userAccess"] == site_id:
            user_list.append(user)
    return user_list


def trigger_mail_box_event(event_type, json_object):
    if event_type == "new":
        event_notification = dict()
        event_notification["eventId"] = json_object["workflowCategoryId"]
        event_notification["siteId"] = json_object["siteId"]
        event_notification["workflowId"] = json_object["workflowId"]
        event_notification["siteName"] = json_object["siteName"]
        event_notification["workflowName"] = json_object["workflowName"]
        event_notification["workflowCategory"] = json_object["workflowCategory"]
        event_notification["timeToRespond"] = json_object["timeToRespond"]
        event_notification["timeToAction"] = json_object["timeToAction"]
        event_notification["workflowStatus"] = json_object["workflowStatus"]
        event_notification["assignedTo"] = json_object["assignedTo"]
        event_notification["assignedToId"] = json_object["assignedToId"]
        event_notification["lastActedBy"] = json_object["lastActedBy"]
        event_notification["eventRead"] = "False"
        event_notification_id = unique_id_generator("eventNotification")
        event_notification["eventNotificationId"] = event_notification_id
        es_obj.update_elastic_search("tnpcb", "event_notification", event_notification_id, event_notification)
        return event_notification
    if event_type == "update":
        event_notification_id = json_object["eventNotificationId"]
        event_notification = es_obj.query_elastic_search_by_id("tnpcb", "event_notification", event_notification_id)
        if "timeToRespond" in json_object:
            event_notification["timeToRespond"] = json_object["timeToRespond"]
        if "workflowStatus" in json_object:
            event_notification["workflowStatus"] = json_object["workflowStatus"]
        if "workflowStatus" in json_object:
            event_notification["assignedTo"] = json_object["assignedTo"]
        if "workflowStatus" in json_object:
            event_notification["assignedToId"] = json_object["assignedToId"]
        event_notification["lastActedBy"] = json_object["lastActedBy"]
        event_notification["eventRead"] = "False"
        es_obj.update_elastic_search("tnpcb", "event_notification", event_notification_id, event_notification)
        return event_notification
    if event_type == "comment":
        event_notification_id = json_object["eventNotificationId"]


def build_query(site_id, parameter_id, monitoring_type, monitoring_id, analyzer_id, start_time, end_time):
    try:
        body_query = '''{"query":{
                        "filtered": {
                             "query": {
                                "match":{
                                        "siteId": "%s"
                                        }
                                       },
                                "filter": {
                                        "range": {
                                "timeCreated" : {
                                "gte" : "%s",
                                "lt"  : "%s"
                            }
                        }}}}}''' % (site_id, start_time, end_time)
        return body_query
    except Exception as e:
        # print "Error while building elastic search query : " + str(e)
        logger.exception("Error while building elastic search query : " + str(e))


def fetch_site_names():
    '''
    This method returns returns the final Json object with Site details mapped through Site ID.
    '''
    query_result = es_obj.query_elastic_search('tnpcb', 'site')
    temp_json = es_obj.fetch_records_from_es_json(query_result)
    # print(tempJson)
    final_json = []
    for record in temp_json:
        final_dict = {}
        final_dict[record['siteInfo']['siteId']] = record['siteInfo']['siteName'] + ',' + record['siteInfo'][
            'city'] + ',' + record['siteInfo']['state']

        final_json.append(final_dict)

    # print finalJson
    return final_json
