import traceback
import pymysql as MySQLdb
from scripts.constants import app_configuration
from scripts.logging.glens_logging import logger as log


class MysqlClient(object):

    def __init__(self):
        self.read_configurations()

    def read_configurations(self):
        self.host = app_configuration.MYSQL_HOST
        self.user = app_configuration.MYSQL_USERNAME
        self.passwd = app_configuration.MYSQL_PASSWORD
        self.db_name = app_configuration.MYSQL_DB_NAME

    def connect(self):
        try:
            conn = MySQLdb.connect(self.host, self.user, self.passwd, self.db_name)
            conn.autocommit(True)
            cur = conn.cursor()
            return conn, cur
        except Exception as e:
            traceback.print_exc()
            log.error("Error while establishing connection : " + str(e))
            return

    def execute(self, query):
        try:
            conn, cursor = self.connect()
            cursor.execute(query)
            result = cursor.fetchall()
            self.close_connection(conn, cursor)
            return result
        except Exception as e:
            traceback.print_exc()
            log.error("Error while executing the query : " + str(e))
            return

    def execute_many(self, query, list):
        conn, cursor = self.connect()
        cursor.executemany(query, list)
        result = cursor.fetchall()
        self.close_connection(conn, cursor)
        return result

    def execute_insert(self, query):
        conn, cursor = self.connect()
        cursor.execute(query)
        return cursor.lastrowid

    def close_connection(self, conn, cur):
        cur.close()
        conn.close()
