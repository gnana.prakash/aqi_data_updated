from scripts.constants import app_configuration
from scripts.constants.app_constants import Dashboard
from scripts.utils.esutility import ESUtility
from scripts.constants.app_configuration import MONGO_PORT, MONGO_HOST, MONGO_DATABASE, SITE_INFO_COLLECTION, \
    MONGO_PASSWORD, MONGO_USERNAME
from scripts.utils.mongoUtility import MongoUtility

mongo_obj = MongoUtility(MONGO_HOST, MONGO_PORT, MONGO_PASSWORD, MONGO_USERNAME)

es_utility = ESUtility(app_configuration.ES_URL)
from scripts.logging.glens_logging import logger as log
import traceback
from datetime import datetime
import json
import requests
import re
import hashlib


def unique_id_generator(field):
    log.info("Running uniqueIdGenerator")
    fieldJson = es_utility.query_elastic_search_by_id("tnpcbconfig", "uniqueid", field)
    if fieldJson == {}:
        fieldJson = {"uniqueId": 1}
        uid = 1
        es_utility.update_elastic_search("tnpcbconfig", "uniqueid", field, fieldJson)
    else:
        uid = fieldJson["uniqueId"]
        uid = uid + 1
        fieldJson = {"uniqueId": uid}
        es_utility.update_elastic_search("tnpcbconfig", "uniqueid", field, fieldJson)
    return field + "_" + str(uid)


# def cmp(a, b):
#     return (a > b) - (a < b)


def check_time_is_greater(date_object1, date_object2):
    try:
        if date_object1 >= date_object2:
            return True
        else:
            return False
    except:
        log.error("Exception caught when comparing the time ->")
        log.error(traceback.format_exc())


# Getting parameters and saving the name and id for appending to the analysers
def get_parameter_id_names():
    tempbodyContent = es_utility.query_elastic_search("tnpcb", "parameter")
    parametersList = es_utility.fetch_records_from_es_json(tempbodyContent)
    finalParameterIdNameList = []
    parameterId = ""
    try:
        for parameter in parametersList:
            tempParam = {}
            tempParam["key"] = parameter["parameterId"]
            parameterId = parameter["parameterId"]
            tempParam["value"] = parameter["parameterName"]
            tempParam["monitoringType"] = parameter["monitoringType"]
            finalParameterIdNameList.append(tempParam)
    except:
        # print "Error when accessing parameterId-->" + parameterId
        log.error("Error when accessing parameterId-->" + parameterId)
        traceback.print_exc()
    return finalParameterIdNameList


# Merge all networks a site falls into
def get_site_and_networks(siteList, networkKeyValueList):
    finalSiteList = []
    for site in siteList:
        networksList = ""
        # print "site"
        # print site
        for network in networkKeyValueList:
            # print "network"
            # print network
            if "selectedSites" in network:
                if site["siteId"] in network["selectedSites"]:
                    networksList = network["value"] + "," + networksList
        if len(networksList) != 0:
            networksList = networksList[:-1]
        site["networks"] = networksList
        networksList = ""
        finalSiteList.append(site)
    return finalSiteList


# Building the possible key value pairs based on Industry Type, Monitoring Type and Site Id
def reports_request_builder(industryCategory, userId, userType):
    industryCategoryList = industryCategory.split(',')
    site_list = get_site_user_mapping(userId, userType)
    siteJsonTempList = get_site_details()
    finalSiteList = []
    for site in siteJsonTempList:
        try:
            siteId = site["siteId"]
            if siteId in site_list:
                if site["industry"] in industryCategoryList or 'All' in industryCategoryList:
                    tempSite = {}
                    tempSite["siteName"] = site["siteName"]
                    tempSite["industry"] = site["industry"]
                    tempSite["city"] = site["city"]
                    tempSite["siteId"] = siteId
                    finalSiteList.append(tempSite)
        except:
            log.error("Exception in reportsRequestBuilder")
    return finalSiteList


# Fetching Network Key Value from the Networks List
def get_network_key_value_list(networksList):
    primaryNetworkKeyValueList = []
    customNetworkKeyValueList = []
    completeNetworkKeyValueList = []
    # print "networksList"
    # print networksList
    for network in networksList:
        networkKeyValue = {}
        if network["parentNetwork"]["key"] == "primary":
            networkKeyValue["key"] = network["networkId"]
            networkKeyValue["value"] = network["networkName"]
            networkKeyValue["parentNetwork"] = network["parentNetwork"]["value"]
            networkKeyValue["parentNetworkId"] = network["parentNetwork"]["value"]
            networkKeyValue["selectedSites"] = []
            if "selectedSites" in network:
                networkKeyValue["selectedSites"] = network["selectedSites"]
            primaryNetworkKeyValueList.append(networkKeyValue)
        else:
            networkKeyValue["key"] = network["networkId"]
            networkKeyValue["value"] = network["networkName"]
            networkKeyValue["parentNetwork"] = network["parentNetwork"]["value"]
            networkKeyValue["parentNetworkId"] = network["parentNetwork"]["value"]
            networkKeyValue["selectedSites"] = []
            if "selectedSites" in network:
                networkKeyValue["selectedSites"] = network["selectedSites"]
            customNetworkKeyValueList.append(networkKeyValue)
        completeNetworkKeyValueList.append(networkKeyValue)
    networkKeyValueList = {}
    networkKeyValueList["primaryNetworkKeyValueList"] = primaryNetworkKeyValueList
    networkKeyValueList["customNetworkKeyValueList"] = customNetworkKeyValueList
    networkKeyValueList["completeNetworkKeyValueList"] = completeNetworkKeyValueList
    return networkKeyValueList


def update_site_graph(siteId):
    jsonObject = {"siteId": ""}
    jsonObject["siteId"] = siteId
    try:
        response = requests.post(app_configuration.REPORT_GEN_URL, data=json.dumps(jsonObject),
                                 headers=Dashboard.__JSON__HEADERS__)
        # print response.text
        return {"status": "Success"}
    except Exception as e:
        # print "Exception caught for site " + siteId
        log.error("Exception caught for site " + siteId)
        traceback.print_exc()
        return {"status": "Failed"}


def get_site_id_name_mapping():
    siteList = get_site_details()
    finalMap = {}
    siteId = ""
    for site in siteList:
        try:
            if "siteId" in site and site["siteId"] is not None and str(site["siteId"]).strip() != "":
                siteId = site["siteId"]
                finalMap[siteId] = {}
                finalMap[siteId]["siteId"] = siteId
                finalMap[siteId]["siteName"] = site["siteName"]
                finalMap[siteId]["city"] = site["city"]
                finalMap[siteId]["industry"] = site["industry"]
        except:
            log.error("Exception caught in getSiteIdNameMapping for siteId-->" + siteId)
    return finalMap


def get_monitoring_label_mapping(monitoringList, monitoringId, monitoringType):
    monitoringLabel = ""
    try:
        for monitoringUnit in monitoringList:
            if monitoringId == monitoringUnit["monitoringId"] and monitoringType == monitoringUnit["monitoringType"]:
                return monitoringUnit["monitoringLabel"]
    except:
        log.error("Error in getMonitoringLabelMapping")
    return monitoringLabel


# This method is in Dashboard, Metastore and Status
def parameters_builder(siteId, isApprovalSite="False"):
    # global SITE_APPROVAL
    try:
        siteFinalJson = {}
        ParameterMap = {}
        if app_configuration.SITE_APPROVAL == "True" and isApprovalSite == "True":
            siteJson = es_utility.query_elastic_search_by_id("tnpcb", "temp_site", siteId)
        else:
            siteJson = es_utility.query_elastic_search_by_id("tnpcb", "site", siteId)
        siteName = siteJson["siteInfo"]["siteName"]
        siteLabel = siteJson["siteInfo"]["siteLabel"]
        city = siteJson["siteInfo"]["city"]
        industry = siteJson["siteInfo"]["industry"]
        latitude = siteJson["siteInfo"]["latitude"]
        longitude = siteJson["siteInfo"]["longitude"]
        siteIp = ""
        if "acquisitionSetup" in siteJson and "ipAddress" in siteJson["acquisitionSetup"]:
            siteIp = siteJson["acquisitionSetup"]["ipAddress"]
        siteStatusJson = es_utility.query_elastic_search_by_id("site_status", "alarms", siteId)
        lastSynchronized = ""
        siteConfigLastUpdatedTime = ""
        lastCalibratedOn = ""
        SiteStatus = ""
        if "lastSynchronized" in siteStatusJson and siteStatusJson["lastSynchronized"] != "":
            lastSynchronized = siteStatusJson["lastSynchronized"]
        if "siteConfigLastUpdatedTime" in siteStatusJson and siteStatusJson["siteConfigLastUpdatedTime"] != "":
            siteConfigLastUpdatedTime = siteStatusJson["siteConfigLastUpdatedTime"]
        if "lastCalibratedOn" in siteStatusJson and siteStatusJson["lastCalibratedOn"] != "":
            lastCalibratedOn = siteStatusJson["lastCalibratedOn"]
        else:
            lastCalibratedOn = siteConfigLastUpdatedTime
        if "SiteStatus" in siteStatusJson and siteStatusJson["SiteStatus"] != "":
            SiteStatus = siteStatusJson["SiteStatus"]

        if "Message" in siteStatusJson:
            siteCurrentStatus = siteStatusJson["Message"]
        else:
            log.error("Site Status message not found")
            siteCurrentStatus = ""
        siteDetails = {}
        siteDetails["siteId"] = siteId
        siteDetails["siteName"] = siteName
        siteDetails["siteLabel"] = siteLabel
        siteDetails["city"] = city
        siteDetails["industry"] = industry
        siteDetails["siteIp"] = siteIp
        siteDetails["lastSynchronized"] = lastSynchronized
        siteDetails["SiteStatus"] = SiteStatus
        siteDetails["latitude"] = latitude
        siteDetails["longitude"] = longitude
        siteDetails["siteConfigLastUpdatedTime"] = siteConfigLastUpdatedTime
        siteDetails["siteCurrentStatus"] = siteCurrentStatus
        siteDetails["lastCalibratedOn"] = lastCalibratedOn
        siteDetails["AcquisitionSystem"] = siteJson["acquisitionSetup"]["acquisitionSystemModel"]
        siteDetails["AcquisitionVersion"] = siteJson["acquisitionSetup"]["acquisitionSystemVersion"]
        monitoringTypeJson = siteJson["sensorSetup"]["subvalues"]
        monitoringList = siteJson["acquisitionSetup"]["monitoringDetails"]
        for monitoringType in monitoringTypeJson:
            for monitoringUnit in monitoringTypeJson[monitoringType]:
                monitoringId = monitoringUnit["monitoringId"]
                for analyser in monitoringUnit["analyser"]:
                    analyzerId = analyser["key"]
                    analyzerName = analyser["value"]
                    for parameter in analyser["parameterDetails"]:
                        parameterId = parameter["key"]
                        key = monitoringType + "." + monitoringId + "." + analyzerId + "." + parameterId

                        finalParameter = {}
                        finalParameter["siteId"] = siteId
                        finalParameter["siteName"] = siteName
                        finalParameter["city"] = city
                        finalParameter["industry"] = industry
                        finalParameter["monitoringType"] = monitoringType
                        finalParameter["monitoringId"] = monitoringId
                        finalParameter["analyzerId"] = analyzerId
                        finalParameter["analyzerName"] = analyzerName
                        finalParameter["parameterId"] = parameterId
                        finalParameter["GaugeMinimum"] = parameter["deviceRangeLowerLimit"]
                        finalParameter["GaugeMaximum"] = parameter["deviceRangeUpperLimit"]
                        finalParameter['counterRangeLowerLimit'] = parameter['counterRangeLowerLimit']
                        finalParameter['counterRangeUpperLimit'] = parameter['counterRangeUpperLimit']
                        finalParameter["aCoefficient"] = parameter["aCoefficient"]
                        finalParameter["bCoefficient"] = parameter["bCoefficient"]
                        finalParameter["compPort"] = parameter["compPort"]
                        finalParameter["ChannelNo"] = parameter["channelNo"]
                        finalParameter["alarmsMaxThreshold"] = parameter["alarmsMaxThreshold"]["rangeValue"]
                        finalParameter["alarmsMaxThresholdId"] = parameter["alarmsMaxThreshold"]["alarmModel"]["key"]
                        finalParameter["alarmsMaximum"] = parameter["alarmsMaximum"]["rangeValue"]
                        finalParameter["alarmsMinimum"] = parameter["alarmsMinimum"]["rangeValue"]
                        finalParameter["alarmsMinThreshold"] = parameter["alarmsMinThreshold"]["rangeValue"]
                        finalParameter["unit"] = parameter["units"]
                        finalParameter["representativenessRate"] = parameter["aggregationCalculationsRepRate"]
                        finalParameter["parameterName"] = parameter["value"]
                        try:
                            finalParameter["cameraUrl"] = parameter["cameraUrl"]
                        except:
                            pass
                        try:
                            finalParameter['cameraName'] = parameter['cameraName']
                        except:
                            pass
                        try:
                            finalParameter["cameraDetails"] = parameter["cameraDetails"]
                        except:
                            pass
                        finalParameter["monitoringLabel"] = get_monitoring_label_mapping(monitoringList, monitoringId,
                                                                                         monitoringType)
                        finalParameter["lastUpdated"] = "2014-01-01T00:00:00Z"
                        finalParameter["lastValidated"] = "2016-01-01T00:00:00Z"
                        finalParameter["lastValidationSaved"] = "2016-01-01T00:00:00Z"
                        try:
                            finalParameter["lastUpdated"] = siteStatusJson[key + ".lastUpdated"]
                        except:
                            pass
                        try:
                            finalParameter["lastValidated"] = siteStatusJson[key + ".lastValidated"]
                        except:
                            pass
                        try:
                            finalParameter["lastValidationSaved"] = siteStatusJson[key + ".lastValidationSaved"]
                        except:
                            pass

                        lastValidationSavedObj = datetime.strptime(finalParameter["lastValidationSaved"],
                                                                   '%Y-%m-%dT%H:%M:%SZ')
                        lastValidatedObj = datetime.strptime(finalParameter["lastValidated"], '%Y-%m-%dT%H:%M:%SZ')
                        if (check_time_is_greater(lastValidatedObj, lastValidationSavedObj)):
                            finalParameter["lastValidationSaved"] = finalParameter["lastValidated"]

                        finalParameter["key"] = key
                        finalParameter["enableParameter"] = "True"
                        if ("enableParameter" in parameter and parameter["enableParameter"] is not None and
                                str(parameter["enableParameter"]).strip() != ""):
                            finalParameter["enableParameter"] = parameter["enableParameter"]
                        ParameterMap[key] = finalParameter
        siteFinalJson["siteDetails"] = siteDetails
        siteFinalJson["parameterMap"] = ParameterMap
        siteFinalJson["siteJson"] = siteJson
        return siteFinalJson
    except Exception as e:
        traceback.print_exc()
        log.error("Error occurred in the parameter builder : %s", str(e), exc_info=True)


# Getting the basic site information for the Site Dashboard Loading. THe method is in Metastore,Dashboard, and Status.

def parameter_label_builder(siteId, isApprovalSite='False'):
    final_mapping = dict()
    if app_configuration.SITE_APPROVAL == "True" and isApprovalSite == "True":
        siteJson = es_utility.query_elastic_search_by_id("tnpcb", "temp_site", siteId)
    else:
        siteJson = es_utility.query_elastic_search_by_id("tnpcb", "site", siteId)

    monitoringTypeJson = siteJson["sensorSetup"]["subvalues"]
    monitoringList = siteJson["acquisitionSetup"]["monitoringDetails"]
    for monitoringType in monitoringTypeJson:
        if monitoringType not in final_mapping:
            final_mapping[monitoringType] = dict()
        for monitoringUnit in monitoringTypeJson[monitoringType]:
            if monitoringUnit['monitoringId'] not in final_mapping[monitoringType]:
                final_mapping[monitoringType][monitoringUnit['monitoringId']] = dict()
            monitoringId = monitoringUnit["monitoringId"]
            for analyser in monitoringUnit["analyser"]:
                analyzerId = analyser["key"]
                analyzerName = analyser["value"]
                if analyzerId not in final_mapping[monitoringType][monitoringUnit['monitoringId']]:
                    final_mapping[monitoringType][monitoringUnit['monitoringId']][analyzerId] = {
                        "analyzerName": analyzerName, "parameters": dict()}
                for parameter in analyser["parameterDetails"]:
                    # print("parameter", parameter)
                    parameterId = parameter["key"]
                    final_mapping[monitoringType][monitoringUnit['monitoringId']][analyzerId]['parameters'][
                        parameterId] = parameter['value']
    print(json.dumps(final_mapping))
    return final_mapping


def get_site_details(isEditCheck=None):
    # tempbodyContent = es_utility.query_elastic_search("tnpcb", "site")
    ESQuery = {"_source": "siteInfo"}
    if isEditCheck is not None and isEditCheck == "True":
        tempbodyContent = es_utility.query_elastic_search_by_body("tnpcb", "temp_site", ESQuery)
    else:
        tempbodyContent = es_utility.query_elastic_search_by_body("tnpcb", "site", ESQuery)
    siteListJson = es_utility.fetch_records_from_es_json(tempbodyContent)
    ESQuery = {"_source": "siteId"}
    tempbodyContent = es_utility.query_elastic_search_by_body("tnpcb", "mobile_site", ESQuery)
    mobileSiteListJson = es_utility.fetch_records_from_es_json(tempbodyContent)
    mobileSiteList = []
    for mobileSite in mobileSiteListJson:
        mobileSiteList.append(mobileSite["siteId"])
    finalSiteList = []
    siteId = ""
    for site in siteListJson:
        try:
            tempSiteInfo = {}
            if "siteInfo" in site:
                siteInfo = site["siteInfo"]
                # print siteInfo
                siteId = siteInfo["siteId"]
                siteInfo = site["siteInfo"]
                tempSiteInfo["siteName"] = siteInfo["siteName"]
                if "siteLabel" in siteInfo:
                    tempSiteInfo["siteLabel"] = siteInfo["siteLabel"]
                else:
                    tempSiteInfo["siteLabel"] = " "
                tempSiteInfo["isMobileSite"] = False
                if siteId in mobileSiteList:
                    tempSiteInfo["isMobileSite"] = True
                tempSiteInfo["industry"] = siteInfo["industry"]
                tempSiteInfo["city"] = siteInfo["city"]
                tempSiteInfo["district"] = ""
                if "district" in siteInfo:
                    tempSiteInfo["district"] = siteInfo["district"]
                tempSiteInfo["state"] = siteInfo["state"]
                tempSiteInfo["siteId"] = siteInfo["siteId"]
                tempSiteInfo["isEdited"] = "false"
                tempSiteInfo["lastEdited"] = ""
                if "isEdited" in siteInfo:
                    tempSiteInfo["isEdited"] = siteInfo["isEdited"]
                if "lastEdited" in siteInfo:
                    tempSiteInfo["lastEdited"] = siteInfo["lastEdited"]
                if 'lastUpdatedTime' in siteInfo:
                    tempSiteInfo['lastUpdatedTime'] = siteInfo['lastUpdatedTime']
                if "isFreezed" in siteInfo:
                    tempSiteInfo['isFreezed'] = siteInfo['isFreezed']
                if "latitude" in siteInfo:
                    tempSiteInfo['latitude'] = siteInfo['latitude']
                if "longitude" in siteInfo:
                    tempSiteInfo['longitude'] = siteInfo['longitude']
                finalSiteList.append(tempSiteInfo)
        except:
            # print "Exception caught in getSiteDetails"
            log.error("Exception caught in getSiteDetails for siteId-->" + siteId)
            traceback.print_exc()
    return finalSiteList


def validate_password_complexity(password):
    isValid = False
    try:
        if (len(str(password).strip()) >= 8 and bool(re.search(r'[a-z]', password)) and bool(
                re.search(r'[A-Z]', password))
                and bool(re.search(r'\d', password))):
            isValid = True
        return isValid
    except:
        log.error("Exception in validate_password_complexity")
    return isValid


def get_site_user_mapping(userId, userType, site_details=None):
    site_id_list = []
    try:
        site_details = {}
        # userDetails = es_utility.query_elastic_search_by_id("tnpcb","userSetup",userId)
        if userType in ["Admin", "CallCenter", "Accountant"]:
            site_details = get_site_details()
            for site_id in site_details:
                if site_id["siteId"] not in site_id_list:
                    site_id_list.append(site_id["siteId"])
            return site_id_list
        elif userType == "Regulator" or userType == "SuperRegulator" or userType == "SuperRegulatorAdmin" or userType == "Nodal" or userType == "Vendor":
            network_list = []
            userDetails = es_utility.query_elastic_search_by_id("tnpcb", "userSetup", userId)
            networkId_list = userDetails["networkAccess"]
            tempbodyContent = es_utility.query_elastic_search("tnpcb", "network")
            bodyContent = es_utility.fetch_records_from_es_json(tempbodyContent)
            networkKeyValueMap = es_utility.fetch_network_hierarchy(bodyContent)
            if networkId_list != None:
                for network in networkId_list:
                    if network in networkKeyValueMap:
                        for site in networkKeyValueMap[network]["siteIdList"]:
                            if site not in site_id_list:
                                site_id_list.append(site)
            return site_id_list
        elif userType == "Client":
            userDetails = es_utility.query_elastic_search_by_id("tnpcb", "userSetup", userId)
            if userDetails["userAccess"] not in site_id_list:
                site_id_list.append(userDetails["userAccess"])
            return site_id_list
    except:
        # print "Exception caught when requesting parameter values"
        log.error("Exception caught when requesting parameter values")
        traceback.print_exc()
    return site_id_list


def network_iterator(d, parent_networkId, parent_networkName):
    temp_networkId = ""
    if "networkId" in d:
        temp_networkId = str(d["networkId"])
        if parent_networkId != "":
            # new_networkId = parent_networkId + "--->" + temp_networkId
            new_networkId = parent_networkId + "|" + temp_networkId
        else:
            new_networkId = temp_networkId
    else:
        new_networkId = parent_networkId
    temp_networkName = ""
    if "siteId" not in d and "networkName" in d:
        temp_networkName = d["networkName"]
        if parent_networkName != "":
            # new_networkName = parent_networkName + "--->" + temp_networkName
            new_networkName = parent_networkName + "|" + temp_networkName
        else:
            new_networkName = temp_networkName
    else:
        new_networkName = parent_networkName
    # print new_networkId
    # print new_networkName
    # print parent_networkId
    # print d
    if "siteId" in d:
        if new_networkId not in finalNetworkSiteMapping:
            finalNetworkSiteMapping[new_networkId] = {}
            finalNetworkSiteMapping[new_networkId]["networkName"] = new_networkName
            finalNetworkSiteMapping[new_networkId]["siteIdList"] = {}
            finalNetworkSiteMapping[new_networkId]["siteIdList"][d["siteId"]] = d["siteName"]
            finalNetworkSiteMapping[parent_networkId]["siteIdList"][d["siteId"]] = d["siteName"]
            # print finalNetworkSiteMapping[parent_networkId]["siteIdList"]
        elif d["siteId"] not in finalNetworkSiteMapping[new_networkId]["siteIdList"]:
            # print new_networkId
            # print parent_networkId
            # print d["siteId"]
            finalNetworkSiteMapping[new_networkId]["siteIdList"][d["siteId"]] = d["siteName"]
            finalNetworkSiteMapping[parent_networkId]["siteIdList"][d["siteId"]] = d["siteName"]
            # print finalNetworkSiteMapping[parent_networkId]["siteIdList"]
    for k, v in d.items():
        # print k
        if isinstance(v, dict):
            network_iterator(v, new_networkId, new_networkName)
        elif isinstance(v, list):
            for item in v:
                # print item
                network_iterator(item, new_networkId, new_networkName)


def fetch_network_hierarchy(bodyContent):
    global finalNetworkSiteMapping
    finalNetworkSiteMapping = {}
    try:
        for network in bodyContent:
            network_iterator(network, "", "")
    except:
        log.error("Exception occurred in networkIterator")
    # print "finalNetworkSiteMapping"
    # print finalNetworkSiteMapping
    finalJson = {}
    try:
        for networkKey in finalNetworkSiteMapping:
            tempNetworkList = networkKey.split("|")
            networkNameList = finalNetworkSiteMapping[networkKey]["networkName"].split("|")
            # print tempNetworkList
            prevKey = ""
            prevNetworkName = ""
            iCount = 0
            for network in tempNetworkList:
                if prevKey == "":
                    prevKey = network
                    prevNetworkName = networkNameList[iCount]
                else:
                    prevKey = prevKey + "-->" + network
                    prevNetworkName = prevNetworkName + "-->" + networkNameList[iCount]
                if prevKey not in finalJson:
                    finalJson[prevKey] = {}
                    finalJson[prevKey]["networkName"] = prevNetworkName
                    finalJson[prevKey]["siteIdList"] = {}
                for siteId in finalNetworkSiteMapping[networkKey]["siteIdList"]:
                    if siteId not in finalJson[prevKey]["siteIdList"]:
                        finalJson[prevKey]["siteIdList"][siteId] = finalNetworkSiteMapping[networkKey]["siteIdList"][
                            siteId]
                iCount = iCount + 1
    except:
        log.error("Exception occurred in fetchNetworkHierarchy")
    # print finalJson
    return finalJson


def site_info_metadata_update(headerKey):
    # print headerKey
    try:
        siteInfoJson = es_utility.query_elastic_search_by_id("tnpcbconfig", "site", "siteinfo")
        # time.sleep(2)
        districtList = []
        if headerKey in ["district", "roName", "tehsil"]:
            tempdistrictContent = es_utility.query_elastic_search("tnpcb", "district")
            districtList = es_utility.fetch_records_from_es_json(tempdistrictContent)
        for header in siteInfoJson["headerContent"]:
            if header["key"] == headerKey:
                if headerKey == "industry":
                    tempIndContent = es_utility.query_elastic_search("tnpcb", "industryCat")
                    industryCatList = es_utility.fetch_records_from_es_json(tempIndContent)
                    options = []
                    for industryCat in industryCatList:
                        options.append(industryCat["industryCategory"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                    break
                if headerKey == "vendor":
                    options = []
                    tempvendorContent = es_utility.query_elastic_search("tnpcb", "vendor")
                    vendortList = es_utility.fetch_records_from_es_json(tempvendorContent)
                    for vendor in vendortList:
                        options.append(vendor["vendorName"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                    break
                if headerKey == "roName":
                    options = []
                    for district in districtList:
                        options.append(district["regionalOffice"])
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                if headerKey == "tehsil":
                    options = []
                    for districtObj in districtList:
                        if "tehsil" in districtObj:
                            for tehsil in districtObj["tehsil"].split(","):
                                if tehsil is not None and str(tehsil).strip() != "" and str(
                                        tehsil).strip() not in options:
                                    options.append(str(tehsil).strip())
                    if options is not None and options != []:
                        header["options"] = sorted(options)
                if headerKey == "district":
                    options = []
                    for districtObj in districtList:
                        if "district" in districtObj:
                            for district in districtObj["district"].split(","):
                                if district is not None and str(district).strip() != "" and str(
                                        district).strip() not in options:
                                    options.append(str(district).strip())
                    if options is not None and options != []:
                        header["options"] = sorted(options)
        # print siteInfoJson
        es_utility.update_elastic_search("tnpcbconfig", "site", "siteinfo", siteInfoJson)
    except:
        log.error("Exception in siteInfoMetadataUpdate")


# Getting the Units and saving it in a list for Configuration page
def get_units_list():
    tempbodyContent = es_utility.query_elastic_search("tnpcb", "unit")
    unitsList = es_utility.fetch_records_from_es_json(tempbodyContent)
    finalunitsList = []
    for unit in unitsList:
        finalunitsList.append(unit["unitValue"])
    return finalunitsList


def user_audit_log_writer(userId, userName, userType, userAccess, actionType, siteId, requestPath, ipAddress, jsonObj,change_list,
                          sourceJson={}):
    try:
        userJson = {}
        userJson["userId"] = userId
        userJson["userName"] = userName
        userJson["userType"] = userType
        userJson["userAccess"] = userAccess
        userJson["actionType"] = actionType
        userJson["requestPath"] = requestPath
        userJson["siteId"] = siteId
        userJson["ipAddress"] = ipAddress
        userJson["jsonObj"] = jsonObj
        userJson["changeList"] = change_list
        userJson["sourceJson"] = sourceJson
        current_time = datetime.now()
        _id = current_time.strftime("%Y%m%d%H%M%S")
        userJson["lastUpdatedAt"] = current_time.strftime("%Y-%m-%dT%H:%M:%SZ")
        es_utility.update_elastic_search("statistics", "user_audit", userId + "_" + _id, userJson)
    except:
        log.error("Exception when updating userAuditLogWriter")

def audit_site_setup_changes(jsonObject, sessionId, userName, site_info):
    current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    siteId = jsonObject["siteId"]
    # siteId = "site_1952"
    final_json = {"status": "failed"}
    try:
        temp_json = dict()
        # temp_json["status"] = "pending"
        temp_json["siteId"] = siteId
        temp_json["siteSessionId"] = sessionId
        temp_json["lastUpdatedBy"] = userName
        temp_json['lastUpdatedTime'] = current_time
        temp_json['siteName'] = site_info['siteName']
        temp_json['industry'] = site_info['industry']
        temp_json['state'] = site_info['state']
        temp_json['epoch_time'] = datetime.now().timestamp()
        temp_json['city'] = site_info['city']
        # temp_json['siteName']
        for each_section in jsonObject["data"]:
            if not len(list(each_section.keys())):
                continue
            else:
                if each_section["section"] == "Acquisition Setup":
                    temp_json["acquisitionSetup"] = each_section["tableData"]["bodyContent"]
                if each_section["section"] == "Site Info":
                    temp_json["siteInfo"] = each_section["tableData"]["bodyContent"]
                if each_section["section"] == "Site Setup":
                    temp_json["siteSetup"] = each_section['tableData']["bodyContent"]
        final_json = temp_json
        try:
            query_site_info = {"siteId": siteId}
            log.debug("Query used to fetch the site info is : %s", query_site_info)
            response = mongo_obj.read(query_site_info, MONGO_DATABASE, SITE_INFO_COLLECTION)
            if response.count():
                mongo_obj.update_one(query_site_info, final_json, MONGO_DATABASE, SITE_INFO_COLLECTION)
            else:
                mongo_obj.insert_one(final_json, MONGO_DATABASE, SITE_INFO_COLLECTION)
            log.debug("New Record inserted")
        except Exception as e:
            log.exception("Exception " + str(e))
        final_json["status"] = "success"
    except Exception as e:
        log.exception("Exception " + str(e))
    return final_json


def get_hash_password(user_password):
    return hashlib.sha256(user_password.encode()).hexdigest()


def check_password(hashed_password, user_password):
    return hashed_password == hashlib.sha256(user_password.encode()).hexdigest()
