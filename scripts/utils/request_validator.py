"""
RequestValidator
"""
# -----------------Start of Import statements------------------------ #
import json
import base64
import traceback
import pytz
from datetime import datetime
from scripts.utils.encryption_utils import EncryptionUtils
from scripts.utils.simple_encrytion_utils import SimpleEncrytionUtils
from scripts.logging.glens_logging import logger as log
from scripts.utils.metadatareader import read_keymetadata_file, get_key_details
from configparser import ConfigParser

# -----------------end of Import statements------------------------ #

COOKIE_KEY = "R0xlbnNTZXJ2ZXIsdmVyXzEuMSxDb29raWUsMjAxNi0wNi0yOS0wMDowMDowMA==################################"
CONFIGURATION_FILE = "conf/application.conf"
session_timeout = 120
site_key_meta_data = read_keymetadata_file()
encryption_utils = EncryptionUtils()
simpleenc_utils = SimpleEncrytionUtils()


def _decode_list(data):
    """
        This function is to _decode_list
        :param data:
        :return rv:
    """
    rv = data
    return rv


def _decode_dict(data):
    """
        This function is to _decode_dict
        :param data:
        :return rv:
    """
    rv = data
    return rv


def read_configurations():
    """
        This function is for read_configurations
        :return:
    """
    try:
        global site_key_meta_data
        global encryption_utils
        global simpleenc_utils
        global session_timeout
        parser = ConfigParser()
        parser.read(CONFIGURATION_FILE)
        try:
            session_timeout = float(parser.get('ServiceSettings', 'session_timeout'))
        except Exception as e:
            log.exception("taking session_timeout as 120 min " + str(e))
            session_timeout = 120

    except Exception as e:
        log.execption("Exception found in reading Configuration" + str(e))
        traceback.print_exc()


# This method will decrypt the signature using the private key and verify the values
def verify_signature(time_stamp, signature, site_id):
    """
        This function is to verify_signature
        :param time_stamp:
        :param signature:
        :param site_id:
        :return:
    """
    global site_key_meta_data
    is_valid = False
    site_key_json = get_key_details(site_id, site_key_meta_data)
    if site_key_json is None or site_key_json == {} or site_key_json == [] or "privateSiteKey" not in site_key_json or \
            "softwareVersion" not in site_key_json:
        return is_valid
    try:
        decrypted_data = encryption_utils.decrypt(signature, encryption_utils.str_to_key(
            site_key_json["privateSiteKey"]))
        signature_data = decrypted_data.split('|')
        current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        current_time = datetime.strptime(current_time, '%Y-%m-%dT%H:%M:%SZ')
        time_stamp_obj = datetime.strptime(time_stamp, '%Y-%m-%dT%H:%M:%SZ')
        difference = current_time - time_stamp_obj
        difference_in_seconds = (difference.microseconds + (
                difference.seconds + difference.days * 24 * 3600) * 10 ** 6) / 10 ** 6
        site_id_data = signature_data[0]
        software_version_id = signature_data[1]
        time_stamp_data = signature_data[2]
        if (site_id == site_id_data and str(time_stamp) == str(time_stamp_data)) and \
                (int(difference_in_seconds) < float(session_timeout)) and \
                site_key_json["softwareVersion"] == software_version_id:
            is_valid = True
        return is_valid
    except Exception as e:
        log.exception("Exception occurred while validating the signature : " + str(e))
        traceback.print_exc()
    return is_valid


# This method will decrypt the signature using the private key and verify the values
def verify_simple_signature(time_stamp, signature, site_id):
    """
        This function is to verify_simple_signature
        :param time_stamp:
        :param signature:
        :param site_id:
        :return:
    """
    global site_key_meta_data
    global simpleenc_utils
    is_valid = False
    site_key_json = get_key_details(site_id, site_key_meta_data)
    error_type = "TYPE1"
    if site_key_json is None or site_key_json == {} or site_key_json == [] or "privateSiteKey" not in site_key_json or \
            "softwareVersion" not in site_key_json:
        return is_valid, error_type
    try:
        error_type = "TYPE2"
        decrypted_data = simpleenc_utils.decrypt(site_key_json["privateSiteKey"], signature)
        signature_data = decrypted_data.split(',')
        current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        current_time = datetime.strptime(current_time, '%Y-%m-%dT%H:%M:%SZ')
        time_stamp_obj = datetime.strptime(time_stamp, '%Y-%m-%dT%H:%M:%SZ')
        difference = current_time - time_stamp_obj
        difference_in_seconds = int(
            (difference.microseconds + (difference.seconds + difference.days * 24 * 3600) * 10 ** 6) / 10 ** 6)
        site_id_data = signature_data[0]
        software_version_id = signature_data[1]
        time_stamp_data = signature_data[2]
        if site_id == site_id_data:
            if str(time_stamp) == str(time_stamp_data):
                if site_key_json["softwareVersion"] == software_version_id:
                    if 0 <= difference_in_seconds < 5 * 60:
                        is_valid = True
                    else:
                        error_type = "TYPE6"
                else:
                    error_type = "TYPE5"
            else:
                error_type = "TYPE4"
        else:
            error_type = "TYPE3"
        return is_valid, error_type
    except Exception as e:
        log.exception("Exception occurred while validating the signature : " + str(e))
        traceback.print_exc()
        error_type = "TYPE7"
    return is_valid, error_type


def check_authorization(headers, enc_type):
    """
        This function is to check_authorization
        :param headers:
        :param enc_type:
        :return:
    """
    return_json = {"status": "Failed", "statusMessage": "ERROR 1001", "code": 500}
    try:
        is_valid = ""
        # LOGIC TO CHECK IF TIMESTAMP IS IN THE REQUEST HEADER
        if "Timestamp" in headers and headers["Timestamp"] is not None \
                and str(headers["Timestamp"]).strip() != "":
            time_stamp = headers["Timestamp"]
        else:
            return_json["statusMessage"] = "ERROR 1001"
            return_json["code"] = 500
            return return_json
        # LOGIC TO CHECK IF Authorization IS IN THE REQUEST HEADER
        if "Authorization" in headers and headers["Authorization"] is not None \
                and str(headers["Authorization"]).strip() != "" \
                and len(headers["Authorization"].split(" ")) == 2:
            authorization = headers["Authorization"].split(" ")[1]
        else:
            return_json["statusMessage"] = "ERROR 1002"
            return_json["code"] = 500
            return return_json
        if enc_type == "TYPE1":
            if "siteId" in headers and headers["siteId"] is not None and str(headers["siteId"]).strip() != "":
                site_id = headers["siteId"]
            else:
                return_json["statusMessage"] = "ERROR 1003"
                return_json["code"] = 500
                return return_json
        elif enc_type == "TYPE2":
            if "Siteid" in headers and headers["Siteid"] is not None and str(headers["Siteid"]).strip() != "":
                site_id = headers["Siteid"]
            else:
                return_json["statusMessage"] = "ERROR 1003"
                return_json["code"] = 500
                return return_json
        if time_stamp is not None and time_stamp.strip() != "" \
                and authorization is not None and authorization.strip() != "" \
                and site_id is not None and site_id.strip() != "":
            if enc_type == "TYPE1":
                is_valid, error_type = verify_simple_signature(time_stamp, authorization, site_id)
            elif enc_type == "TYPE2":
                is_valid = verify_signature(time_stamp, authorization, site_id)
        else:
            return_json["statusMessage"] = "ERROR 1004"
            return_json["code"] = 500
            return return_json
        if is_valid is None or is_valid == "":
            return_json["statusMessage"] = "ERROR 1005 " + error_type
            return_json["code"] = 500
            return return_json
        elif is_valid is False:
            return_json["statusMessage"] = "ERROR 1006 " + error_type
            return_json["code"] = 500
            return return_json
        elif is_valid is True:
            return_json["status"] = "Success"
            return_json["statusMessage"] = "Success"
            return_json["code"] = 200
            return return_json
        else:
            return_json["statusMessage"] = "ERROR 1007"
            return_json["code"] = 500
            return return_json
    except Exception as e:
        log.exception("Exception occurred in post method " + str(e))
        traceback.format_exc()
        return_json["statusMessage"] = "ERROR 1008"
        return_json["code"] = 500
        return return_json


def create_cookie(user_name, user_id, ip_address, user_agent, server_category):
    """
        This function is to create_cookie
        :param user_name:
        :param user_id:
        :param ip_address:
        :param user_agent:
        :param server_category:
        :return:
    """
    cookie_creation = False
    cookie_enc_str = ""
    ip_address = "1.6.111.127"
    if user_name is not None and user_name.strip() != "" and ip_address is not None and ip_address.strip() != "" and \
            user_agent is not None and user_agent.strip() != "" and user_id is not None and user_id.strip() != "" and \
            server_category is not None and server_category.strip() != "":
        try:
            valid_till_obj = datetime.now()
            cookie_str = (user_name + "^" + user_id + "^" + ip_address + "^" + user_agent + "^" +
                          server_category + "^" +
                          valid_till_obj.strftime('%Y-%m-%dT%H:%M:%SZ'))
            cookie_enc_str = simpleenc_utils.encrypt(COOKIE_KEY, cookie_str)
            cookie_creation = True
            return cookie_creation, cookie_enc_str
        except Exception as e:
            log.exception("Exception Creating Cookie" + str(e))
    return cookie_creation, cookie_enc_str


def validate_cookie(cookied_enc_str, headers, json_enc_string, server_category):
    """
        This function is to validate_cookie
        :param cookied_enc_str:
        :param headers:
        :param json_enc_string:
        :param server_category:
        :return:
    """
    is_valid = False
    json_object = {}
    try:
        log.info("Validating the cookie now")
        json_string = base64.b64decode(json_enc_string)
        log.debug("Input data for cookie validation : %s", json_string)
        user_id = json.loads(json_string)["userId"]
        ip_address = headers.get("X-Forwarded-For")
        user_agent = headers.get("User-Agent")
        ip_address = "1.6.111.127"
        try:
            decrypted_str = simpleenc_utils.decrypt(COOKIE_KEY, cookied_enc_str)
            log.debug("Decryped string : %s", decrypted_str)
        except Exception as e:
            log.error("Error while decrypting the cookie : %s", str(e), exc_info=True)
            raise Exception(str(e))
        decrypted_str = decrypted_str.decode("utf-8")
        decrypted_str_list = decrypted_str.split("^")
        if user_id is None or user_id.strip() == "" and ip_address is None or ip_address.strip() == "" or \
                user_agent is None or user_agent.strip() == "" or server_category is None or \
                server_category.strip() == "":
            return is_valid, json_object
        time_stamp = decrypted_str_list[5]
        current_time_str = datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ")
        # current_time = datetime.now(pytz.timezone('Asia/Calcutta')).strftime("%Y-%m-%dT%H:%M:%SZ")
        current_time = datetime.strptime(current_time_str, '%Y-%m-%dT%H:%M:%SZ')
        time_stamp_obj = datetime.strptime(time_stamp, '%Y-%m-%dT%H:%M:%SZ')
        difference = current_time - time_stamp_obj
        difference_in_seconds = int(
            (difference.microseconds + (difference.seconds + difference.days * 24 * 3600) * 10 ** 6) / 10 ** 6)
        log.debug('Current TS - {}, Request TS - {}, Diff in secs - {} | Session timeout - {}'.format(current_time_str, time_stamp, difference_in_seconds, float(session_timeout) * 60))
        if difference_in_seconds < 0 or difference_in_seconds >= float(session_timeout) * 60:
            log.debug("Within return-->%s", str(is_valid))
            return is_valid, json_object
        # if (user_id == decrypted_str_list[1] and ip_address == decrypted_str_list[2] and
        #         user_agent == decrypted_str_list[3] and
        #         server_category == decrypted_str_list[4] and 0 <= difference_in_seconds < float(session_timeout) * 60):
        log.debug('User id - {} | Validate user id - {}'.format(user_id, decrypted_str_list[1]))
        log.debug('IP - {} | Validate IP - {}'.format(ip_address, decrypted_str_list[2]))
        log.debug('User Agent - {} | Validate user agent - {}'.format(user_agent, decrypted_str_list[3]))
        log.debug('Server category - {} | Validate server - {}'.format(server_category, decrypted_str_list[4]))
        log.debug('Current TS - {}, Request TS - {}, Diff in secs - {} | Session timeout - {}'.format(current_time_str, time_stamp, difference_in_seconds, float(session_timeout) * 60))
        if (user_id == decrypted_str_list[1] and ip_address == decrypted_str_list[2] and user_agent ==decrypted_str_list[3] and server_category == decrypted_str_list[
                    4] and 0 <= difference_in_seconds < float(session_timeout) * 60):
            is_valid = True
            log.debug("Within 2nd return-->%s", str(is_valid))
            json_object = json.loads(json_string, object_hook=_decode_dict)
            return is_valid, json_object
        else:
            log.error("-----ELSE BLOCK--------")
            print("Failed")
    except Exception as e:
        log.error("Error while validating the cookie : %s", str(e), exc_info=True)
        raise Exception(str(e))
    return is_valid, json_object


def validate_cookie_normal(cookied_enc_str, headers, json_enc_string, server_category):
    """
        This function is to validate_cookie_normal
        :param cookied_enc_str:
        :param headers:
        :param json_enc_string:
        :param server_category:
        :return:
    """
    is_valid = False
    json_object = {}
    try:
        json_string = base64.b64decode(json_enc_string)
        ip_address = headers["X-Forwarded-For"]
        user_agent = headers["User-Agent"]
        decrypted_str = simpleenc_utils.decrypt(COOKIE_KEY, cookied_enc_str)
        decrypted_str_list = decrypted_str.split("^")
        time_stamp = decrypted_str_list[5]
        current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        current_time = datetime.strptime(current_time, '%Y-%m-%dT%H:%M:%SZ')
        time_stamp_obj = datetime.strptime(time_stamp, '%Y-%m-%dT%H:%M:%SZ')
        difference = current_time - time_stamp_obj
        difference_in_seconds = int(
            (difference.microseconds + (difference.seconds + difference.days * 24 * 3600) * 10 ** 6) / 10 ** 6)
        if difference_in_seconds < 0 or difference_in_seconds >= float(session_timeout) * 60:
            return is_valid, json_object
        if (ip_address == decrypted_str_list[2] and user_agent == decrypted_str_list[3] and
                server_category == decrypted_str_list[4] and 0 <= difference_in_seconds < float(session_timeout) * 60):
            is_valid = True
            json_object = json.loads(json_string, object_hook=_decode_dict)
            return is_valid, json_object
    except Exception as e:
        log.exception("Exception when validating cookie" + str(e))
    return is_valid, json_object


def validate_proxy_cookie(cookied_enc_str, headers, server_category):
    """
        This function is to validate_proxy_cookie
        :param cookied_enc_str:
        :param headers:
        :param server_category:
        :return:
    """
    is_valid = False
    try:
        ip_address = headers["X-Forwarded-For"]
        user_agent = headers["User-Agent"]
        decrypted_str = simpleenc_utils.decrypt(COOKIE_KEY, cookied_enc_str)
        decrypted_str_list = decrypted_str.split("^")
        if ip_address is None or ip_address.strip() == "" or \
                user_agent is None or user_agent.strip() == "" or server_category is None or \
                server_category.strip() == "":
            return is_valid
        time_stamp = decrypted_str_list[5]
        current_time = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        current_time = datetime.strptime(current_time, '%Y-%m-%dT%H:%M:%SZ')
        time_stamp_obj = datetime.strptime(time_stamp, '%Y-%m-%dT%H:%M:%SZ')
        difference = current_time - time_stamp_obj
        difference_in_seconds = int(
            (difference.microseconds + (difference.seconds + difference.days * 24 * 3600) * 10 ** 6) / 10 ** 6)
        if difference_in_seconds < 0 or difference_in_seconds >= float(session_timeout) * 60:
            return is_valid
        if (ip_address == decrypted_str_list[2] and user_agent == decrypted_str_list[3] and
                server_category == decrypted_str_list[4] and 0 <= difference_in_seconds < float(session_timeout) * 60):
            is_valid = True
            return is_valid
    except Exception as e:
        log.exception("Exception when validating cookie" + str(e))
    return is_valid


read_configurations()
