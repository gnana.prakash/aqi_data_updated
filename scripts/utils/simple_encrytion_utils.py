"""
SimpleEncryptionutils
"""
# -----------------Start of Import statements------------------------ #
import base64
import hashlib

from Crypto import Random
from Crypto.Cipher import AES

# from scripts.logging.glens_logging import logger as log
# -----------------end of Import statements------------------------ #


# Utility class for data encryption
class SimpleEncrytionUtils(object):
    """
        SimpleEncrytionUtils
    """

    # Constructor
    def __init__(self):
        """
            Initializer
        """
        self.BLOCK_SIZE = 32
        self.KEY_SIZE = 32
        self.PADDING = '#'
        self.IV = 16 * '\x00'

    def _pad(self, data, data_type):
        """
        Data to be encrypted should be on 16, 24 or 32 byte boundaries.
        So if you have 'hi', it needs to be padded with 30 more characters
        to make it 32 bytes long. Similary if something is 33 bytes long,
        31 more bytes are to be added to make it 64 bytes long which falls
        on 32 boundaries.
        - BLOCK_SIZE is the boundary to which we round our data to.
        - PADDING is the character that we use to padd the data.
        """
        if data_type == "data":
            return data + (self.BLOCK_SIZE - len(data) % self.BLOCK_SIZE) * self.PADDING
        elif data_type == "key":
            return data + (self.KEY_SIZE - len(data) % self.BLOCK_SIZE) * self.PADDING

    def key_encrypt(self, key_str):
        """
            This function for key_encrypt
            :param key_str:
            :return:
        """
        return self._pad(base64.b64encode(key_str), "key")

    @staticmethod
    def key_decrypt(key_str):
        """
            This function for key_decrypt
            :param key_str:
            :return:
        """
        return base64.b64decode(key_str)

    def encrypt(self, secret_key, data):
        """
            Encrypts the given data with given secret key.
            :param secret_key:
            :param data:
            :return:
        """
        cipher = AES.new(self._pad(secret_key, "data")[:32], AES.MODE_CBC, IV=self.IV)
        return base64.b64encode(cipher.encrypt(self._pad(data, "data")))

    def decrypt(self, secret_key, encrypted_data):
        """
            Decryptes the given data with given key.
            :param secret_key:
            :param encrypted_data:
            :return:
        """
        source = self._pad(secret_key, "data")[:32]
        print(source)
        print(type(source))
        cipher = AES.new(source.encode(), AES.MODE_CBC, IV=self.IV.encode())
        return cipher.decrypt(base64.b64decode(encrypted_data)).rstrip(self.PADDING.encode('utf-8'))


class AESCipher(object):

    def __init__(self, auth_key):
        self.bs = AES.block_size
        self.key = hashlib.sha256(auth_key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw.encode()))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s) - 1:])]
