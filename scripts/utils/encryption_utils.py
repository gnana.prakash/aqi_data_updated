"""
EncryptionUtils
"""
# -----------------Start of Import statements------------------------ #
from __future__ import division, absolute_import, print_function
import sys
import binascii
from math import log
from fractions import gcd
from random import randrange
from collections import namedtuple
from binascii import hexlify, unhexlify

# -----------------end of Import statements------------------------ #


# Utility class for data encryption
class EncryptionUtils(object):
    """
    EncryptionUtils
    """
    # Constructor
    def __init__(self):
        """
            Initializer
        """
        py3 = sys.version_info[0] == 3
        if py3:
            self.binary_type = bytes
            self.range_func = range
        else:
            self.binary_type = str
            self.range_func = xrange
        self.key_pair = namedtuple('KeyPair', 'public private')
        self.key = namedtuple('Key', 'exponent modulus')

    # Rabin Primality Test
    def is_prime(self, n, k=30):
        """
            This function is to check if prime
            :param n:
            :param k:
            :return:
            http://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
        """
        if n <= 3:
            return n == 2 or n == 3
        neg_one = n - 1

        # write n-1 as 2^s*d where d is odd
        s, d = 0, neg_one
        while not d & 1:
            s, d = s + 1, d >> 1
        assert 2 ** s * d == neg_one and d & 1

        for _ in self.range_func(k):
            a = randrange(2, neg_one)
            x = pow(a, d, n)
            if x in (1, neg_one):
                continue
            for _ in self.range_func(s - 1):
                x = x ** 2 % n
                if x == 1:
                    return False
                if x == neg_one:
                    break
            else:
                return False
        return True

    def randprime(self, n=10 ** 8):
        """
            This function is to randprime
            :param n:
            :return:
        """
        p = 1
        while not self.is_prime(p):
            p = randrange(n)
        return p

    @staticmethod
    def multinv(modulus, value):
        """
            Multiplicative inverse in a given modulus
            :param modulus:
            :param value:
            :return:
            multinv(191, 138)
            18
            multinv(191, 38)
            186
            multinv(120, 23)
            47
        """
        # http://en.wikipedia.org/wiki/Extended_Euclidean_algorithm
        x, lastx = 0, 1
        a, b = modulus, value
        while b:
            a, q, b = b, a // b, a % b
            x, lastx = lastx - q * x, x
        result = (1 - lastx * modulus) // value
        if result < 0:
            result += modulus
        assert 0 <= result < modulus and value * result % modulus == 1
        return result

    def keygen(self, n, public=None):
        """
            Generate public and private keys from primes up to N.
            :param n:
            :param public:
            :return:

            Optionally, specify the public key exponent (65537 is popular choice).
            pubkey, privkey = keygen(2**64)
            msg = 123456789012345
            coded = pow(msg, *pubkey)
            plain = pow(coded, *privkey)
            assert msg == plain
        """
        # http://en.wikipedia.org/wiki/RSA
        prime1 = self.randprime(n)
        prime2 = self.randprime(n)
        composite = prime1 * prime2
        totient = (prime1 - 1) * (prime2 - 1)
        if public is None:
            private = None
            while True:
                private = randrange(totient)
                if gcd(private, totient) == 1:
                    break
            public = self.multinv(totient, private)
        else:
            private = self.multinv(totient, public)
        assert public * private % totient == gcd(public, totient) == gcd(private, totient) == 1
        assert pow(pow(1234567, public, composite), private, composite) == 1234567
        return self.key_pair(self.key(public, composite), self.key(private, composite))

    # Method for encrypting the data
    def encrypt(self, msg, pubkey, verbose=False):
        """
            This function is to encrypt
            :param msg:
            :param pubkey:
            :param verbose:
            :return:
        """
        chunksize = int(log(pubkey.modulus, 256))
        outchunk = chunksize + 1
        outfmt = '%%0%dx' % (outchunk * 2,)
        bmsg = msg if isinstance(msg, self.binary_type) else msg.encode('utf-8')
        result = []
        for start in self.range_func(0, len(bmsg), chunksize):
            chunk = bmsg[start:start + chunksize]
            chunk += b'\x00' * (chunksize - len(chunk))
            plain = int(hexlify(chunk), 16)
            coded = pow(plain, *pubkey)
            bcoded = unhexlify((outfmt % coded).encode())
            if verbose:
                print('Encode:', chunksize, chunk, plain, coded, bcoded)
            result.append(bcoded)
        return binascii.b2a_base64(b''.join(result))

    # Method for decrypting the data
    def decrypt(self, msg, privkey, verbose=False):
        """
            This function is to decrypt
            :param msg:
            :param privkey:
            :param verbose:
            :return:
        """
        bcipher = binascii.a2b_base64(msg)
        chunksize = int(log(privkey.modulus, 256))
        outchunk = chunksize + 1
        outfmt = '%%0%dx' % (chunksize * 2,)
        result = []
        for start in self.range_func(0, len(bcipher), outchunk):
            bcoded = bcipher[start: start + outchunk]
            coded = int(hexlify(bcoded), 16)
            plain = pow(coded, *privkey)
            chunk = unhexlify((outfmt % plain).encode())
            if verbose:
                print('Decode:', chunksize, chunk, plain, coded, bcoded)
            result.append(chunk)
        return b''.join(result).rstrip(b'\x00').decode('utf-8')

    # Converting the key to string
    @staticmethod
    def key_to_str(key):
        """
            :param key:
            :return:
            Convert `Key` to string representation
            key_to_str(Key(50476910741469568741791652650587163073, 95419691922573224706255222482923256353))
            '25f97fd801214cdc163796f8a43289c1:47c92a08bc374e96c7af66eb141d7a21'
        """
        return ':'.join((('%%0%dx' % ((int(log(number, 256)) + 1) * 2)) % number) for number in key)

    # Converting a  string to key
    def str_to_key(self, key_str):
        """
            :param key_str:
            :return:
            Convert string representation to `Key` (assuming valid input)
            str_to_key('25f97fd801214cdc163796f8a43289c1:47c92a08bc374e96c7af66eb141d7a21') ==
            ...  Key(exponent=50476910741469568741791652650587163073, modulus=95419691922573224706255222482923256353))
            True
        """
        return self.key(*(int(number, 16) for number in key_str.split(':')))
