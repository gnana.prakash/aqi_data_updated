"""
MetadataReader __author__ = 'Sivaram'
"""
# -----------------Start of Import statements------------------------ #
import json
from scripts.logging.glens_logging import logger as log

# -----------------end of Import statements------------------------ #

site_key_meta_data = {}
site_type_meta_data = {}


def _decode_list(data):
    """
            This function is to _decode_list
            :param data:
            :return rv:
    """
    rv = data
    return rv


def _decode_dict(data):
    """
        This function is to _decode_dict
        :param data:
        :return rv:
    """
    rv = data
    return rv


def read_keymetadata_file():
    """
        This Function is to read_keymetadata_file
        :return:
    """
    file_obj = None
    global site_key_meta_data
    site_key_meta_data = {}
    meta_data_filename = "siteKey.json"
    try:
        file_obj = open(meta_data_filename, 'r')
        site_key_meta_data = json.load(file_obj)
        site_key_meta_data = json.loads(json.dumps(site_key_meta_data), object_hook=_decode_dict)
    except Exception as e:
        log.exception("Exception occurred when reading the metadata file -->" + meta_data_filename + str(e))
        try:
            file_obj.close()
        except Exception as e:
            log.exception("Exception occurred when closing the metadata file -->" + meta_data_filename + str(
                e))
    return site_key_meta_data


def get_key_details(site_id, meta_data):
    """
        This function is to get_key_details
        :param site_id:
        :param meta_data:
        :return:
    """
    try:
        if site_id in meta_data:
            site = meta_data[site_id]
            if "siteId" in site and site["siteId"] is not None and site["siteId"].strip() != "":
                if site_id == site["siteId"]:
                    return site
        return {}
    except Exception as e:
        log.exception("Exception occurred getKeyDetails for siteId-->" + str(site_id) + str(e))


def read_site_type_file():
    """
        This function is to read_site_type_file
        :return:
    """
    file_obj = None
    global site_type_meta_data
    site_type_meta_data = {}
    site_type_filename = "siteMapping.json"
    try:
        file_obj = open(site_type_filename, 'r')
        site_type_meta_data = json.load(file_obj)
        site_type_meta_data = json.loads(json.dumps(site_type_meta_data), object_hook=_decode_dict)
    except Exception as e:
        log.exception("Exception occurred when reading the metadata file -->" + site_type_filename + str(e))
        try:
            file_obj.close()
        except Exception as e:
            log.exception("Exception occurred when closing the metadata file -->" + site_type_filename + str(
                e))
    return site_type_meta_data
