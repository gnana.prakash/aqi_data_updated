'# Author: Chandrakanth'
import boto3
import botocore
from scripts.constants import app_configuration
from scripts.logging.glens_logging import logger as log


class S3Utility:
    def __init__(self):
        if app_configuration.storage_type.lower() == 's3':
            self.conn_s3 = boto3.client('s3', aws_access_key_id=app_configuration.access_key, aws_secret_access_key=app_configuration.secret_key,
                                         region_name=app_configuration.region_name)
        elif app_configuration.storage_type.lower() == 'local':
            self.conn_s3 = boto3.client('s3', aws_access_key_id=app_configuration.access_key,
                                        aws_secret_access_key=app_configuration.secret_key,
                                        config=boto3.session.Config(signature_version='s3v4'), region_name=app_configuration.region_name,
                                        endpoint_url=app_configuration.minio_end_point)

    def upload_file_to_s3(self, file_path, relative_s3_path):
        try:
            log.info("Started uploading file to s3")
            self.conn_s3.upload_file(file_path, app_configuration.bucket_name, relative_s3_path)
        except Exception as e:
            log.info("Failed to upload file to s3", str(e))

    def presign_s3(self, file_path, expiration=3600):
        """
        Function to generate presigned URL on S3 (Default 1 hour expiration)
        :param file_path:
        :param expiration:
        :return:
        """
        try:
            log.info("Started generating the pre signed url")
            params = {
                'Bucket': app_configuration.bucket_name,
                'Key': file_path
            }
            signed_url = self.conn_s3.generate_presigned_url('get_object', Params=params, ExpiresIn=expiration)
            return signed_url
        except Exception as e:
            log.info("Error while generating the pre signed url %s", str(e))
            raise Exception(str(e))

    def make_bucket(self,bucket_name):
        try:
            print("**************")
            self.conn_s3.make_bucket(bucket_name, location=app_configuration.region_name)
            buckets = self.conn_s3.list_buckets()
            print(buckets)
        except Exception as e:
            log.info("failed to make bucket---->"+str(e))

# if __name__ == '__main__':
#     s3_obj = S3Utility()
#     src_path = "C:/Users/Admin/PycharmProjects/glens_sms_report/templates/Report.xlsx"
#     destination_path = 'test_reports/'
#     s3_obj.upload_file_to_s3(src_path, destination_path)
#     signed_url = s3_obj.presign_s3(destination_path)
#     print(signed_url)
