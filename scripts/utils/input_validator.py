import base64
import json

from scripts.constants import app_configuration
from scripts.constants import app_constants
from scripts.logging.glens_logging import logger
from scripts.utils.request_validator import validate_cookie


def validate_input_request(request, input_data):
    try:
        logger.info("Fetch the site info")
        logger.debug("Input data for fetching the site info is : %s", str(input_data))
        json_string = input_data
        server_cat = app_configuration.SERVICE_CAT
        cookie_str = app_constants.Dashboard.COOKIE_STR
        if server_cat != "":
            if request.cookies is not None and cookie_str in request.cookies:
                actual_cookie = request.cookies[cookie_str]
                is_valid_cookie, json_object = validate_cookie(actual_cookie, request.headers, input_data,
                                                               server_cat)
                logger.debug("Cookie status after validation - {}".format(is_valid_cookie))
            else:
                is_valid_cookie = False
                json_object = json.loads(base64.b64decode(json_string.decode('utf-8')))
        else:
            is_valid_cookie = True
            json_object = json.loads(base64.b64decode(json_string.decode('utf-8')))
        logger.debug("Cookies status : %s", is_valid_cookie)
        logger.debug("Input data : %s", json_object)
        if is_valid_cookie:
            logger.info("Session is valid , so fetching the site info")
            return True, json_object
        else:
            return False, {"status": "session_invalid", "message": "Session Expired"}
    except Exception as e:
        logger.error("Error while fetching the site info : %s", str(e), exc_info=True)
        raise Exception({"status": "Failed", "message": "Failed to fetch site info"})
