"""
AUTHOR: GNANA PRAKASH INT-561
"""
import csv
import time
import traceback
from datetime import datetime
from statistics import mean
from scripts.constants import app_configuration
from scripts.logging.glens_logging import logger
from scripts.utils.kairosdbutility import KairosDButlity
from datetime import timedelta
import pandas as pd


class AvgAqiHandler:

    def __init__(self):
        self.kairos_db_obj = KairosDButlity(app_configuration.data_url)

    def avg_aqi_handler(self, json_obj):
        final_json = {"status": False, "message": "Failed to get data"}
        try:
            logger.info("Getting historical aqi data")
            station_list = json_obj["stations"]
            city_list = []
            final_list = []
            for each_id in station_list:
                for site_id, city_name in each_id.items():
                    if city_name not in city_list:
                        city_list.append(city_name)
            for each_city in city_list:
                from_date = json_obj["fromDate"] + " 00:00:00"
                to_date = json_obj["toDate"] + " 23:59:59"
                dup_list = []
                for each_id in station_list:
                    for site_id, city_name in each_id.items():
                        if each_city == city_name:
                            from_date_epoch = int(time.mktime(time.strptime(from_date, "%Y-%m-%d %H:%M:%S"))) * 1000
                            to_date_epoch = int(time.mktime(time.strptime(to_date, "%Y-%m-%d %H:%M:%S"))) * 1000
                            data_response = self.fetch_avg_kairos_data("glens_historic.aqi.data", site_id,
                                                                       from_date_epoch, to_date_epoch)
                            for resp in data_response:
                                logger.info("Checking kairos response data")
                                for value in resp["values"]:
                                    dup_dict = {}
                                    timestamp = value[0] / 1000
                                    dup_dict[datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")] = value[1]
                                    dup_list.append(dup_dict)
                while from_date <= to_date:
                    final_dict = {}
                    avg_list = []
                    for data in dup_list:
                        for timestamp in data:
                            if timestamp == from_date:
                                avg_list.append(data[timestamp])
                    if avg_list:
                        avg_aqi = float(mean(avg_list))
                        from_date_dup = from_date.split(" ")
                        final_dict["Date"] = from_date_dup[0]
                        final_dict["City Name"] = each_city
                        final_dict["AQI"] = round(avg_aqi, 2)
                        if 0.00 <= avg_aqi <= 50.00:
                            final_dict["Remarks"] = "Good"
                        elif 51.00 <= avg_aqi <= 100.00:
                            final_dict["Remarks"] = "Satisfactory"
                        elif 101.00 <= avg_aqi <= 150.00:
                            final_dict["Remarks"] = "Moderate"
                        elif 151.00 <= avg_aqi <= 200.00:
                            final_dict["Remarks"] = "Poor"
                        elif 201.00 <= avg_aqi <= 250.00:
                            final_dict["Remarks"] = "Very Poor"
                        elif 251.00 <= avg_aqi <= 300.00:
                            final_dict["Remarks"] = "Severe"

                    else:
                        from_date_dup = from_date.split(" ")
                        final_dict["Date"] = from_date_dup[0]
                        final_dict["City Name"] = each_city
                        final_dict["AQI"] = "NA"
                        final_dict["Remarks"] = "No Data"

                    from_date = (datetime.strptime(from_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=1)).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    final_list.append(final_dict)
            csv_columns = ['Date', 'City Name', 'AQI', 'Remarks']
            csv_file = r"Report\Avg_AQI.csv"
            try:
                with open(csv_file, 'w') as fp:
                    writer = csv.DictWriter(fp, fieldnames=csv_columns)
                    writer.writeheader()
                    for data in final_list:
                        writer.writerow(data)
            except IOError:
                logger.exception("I/O error")

            spreadsheet = pd.read_csv('Avg_AQI.csv')
            final_excel = pd.ExcelWriter(r'Report\Avg_AQI.xlsx')
            spreadsheet.to_excel(final_excel, index=False)
            final_excel.save()
            final_json["status"] = True
            final_json["message"] = "Success"
            final_json["path"] = r'Report\Avg_AQI.xlsx'

            return final_json
        except Exception as e:
            logger.exception(f"Exception occurred while getting data from kairos: {str(e)}")
            traceback.print_exc()

    def fetch_avg_kairos_data(self, metric_name, site_id, from_date, to_date):
        final_response = []
        try:
            logger.info("*"*20 + "Started hitting kairos" + "*"*20)
            metric_json = [
                {
                    "name": metric_name,
                    "tags": {
                        'site_id': site_id
                    },
                    "group_by": [
                        {
                            "name": "tag",
                            "tags": [
                                "monitoring_id",
                                "parameter_id"
                            ]
                        }
                    ],
                    "aggregators": [{
                        "name": "avg",
                        "align_start_time": True,
                        "sampling": {
                            "value": 1,
                            "unit": "days"
                        }
                    }]
                }
            ]
            response = self.kairos_db_obj.fetch_avg_data(metric_json, from_date, to_date)
            if response:
                for each in response["queries"][0]["results"]:
                    temp = {"site_name": each["tags"]["site_name"][0], "parameter_id": each["tags"]["parameter_id"][0],
                            "monitoring_id": each["tags"]["monitoring_id"][0], "values": each["values"]}
                    final_response.append(temp)
        except Exception as e:
            logger.exception(f"Error occurred while fetching kairos data: {str(e)}")
            traceback.print_exc()
        return final_response


def get_color(value):
    if 0 < value <= 50:
        return "scale-one"
    elif 50 < value <= 100:
        return "scale-two"
    elif 100 < value <= 200:
        return "scale-three"
    elif 200 < value <= 300:
        return "scale-four"
    elif 300 < value <= 400:
        return "scale-five"
    else:
        return "scale-six"


def get_avg_remark(value):
    if 0 < value <= 50:
        return "Good"
    elif 50 < value <= 100:
        return "Satisfactory"
    elif 100 < value <= 200:
        return "Moderate"
    elif 200 < value <= 300:
        return "Poor"
    elif 300 < value <= 400:
        return "Very Poor"
    else:
        return "Severe"
