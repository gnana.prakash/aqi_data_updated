import time
import traceback
from datetime import datetime

from scripts.constants import app_constants as const
from scripts.constants import app_configuration
from scripts.logging.glens_logging import logger
from scripts.utils.kairosdbutility import KairosDButlity


class AqiHandler:
    def __init__(self):
        self.kairos_db_obj = KairosDButlity(app_configuration.data_url)

    def aqi_handler(self, json_obj):
        final_json = {"status": False, "message": "Failed to get data"}
        try:
            logger.info("Getting historical aqi data")
            header_content = [{"key": "name", "value": "Station Name"},
                              {"key": "city", "value": "City"},
                              {"key": "monitoring_type", "value": "Monitoring Type"},
                              {"key": "parameter", "value": "Parameter"},
                              {"key": "aqi", "value": "AQI"},
                              {"key": "remarks", "value": "Remarks"},
                              {"key": "timestamp", "value": "Timestamp"}]
            final_json["headerContent"] = header_content
            from_date = json_obj["fromDate"] + " 00:00:00"
            logger.info(f"from date --> {from_date}")
            to_date = json_obj["toDate"] + " 23:59:59"
            logger.info(f"to date --> {to_date}")
            station_list = json_obj["stations"]
            temp_list = []
            for each_id in station_list:
                for site_id, city_id in each_id.items():
                    from_date_epoch = int(time.mktime(time.strptime(from_date, "%Y-%m-%d %H:%M:%S"))) * 1000
                    to_date_epoch = int(time.mktime(time.strptime(to_date, "%Y-%m-%d %H:%M:%S"))) * 1000
                    data_response = self.fetch_kairos_data("glens_historic.aqi.data", site_id, from_date_epoch,
                                                           to_date_epoch)
                    for resp in data_response:
                        logger.info("Checking kairos response data")
                        temp_dict = {"name": resp["site_name"], "monitoring_type": resp["monitoring_id"],
                                     "parameter": resp["parameter_id"]}
                        for city in const.city_list:
                            if city in temp_dict["name"]:
                                temp_dict["city"] = city
                        for value in resp["values"]:
                            copy_dict = temp_dict.copy()
                            copy_dict["aqi"] = f"<B class={get_color(value[1])}>{value[1]}</B>"
                            copy_dict["remarks"] = get_remark(value[1])
                            timestamp = value[0] / 1000
                            copy_dict["timestamp"] = datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")
                            temp_list.append(copy_dict)
            final_json["bodyContent"] = temp_list
            final_json["status"] = True
            final_json["message"] = "Fetched data successfully."
        except Exception as e:
            logger.exception(f"Exception occurred while getting data from kairos: {str(e)}")
            traceback.print_exc()
        return final_json

    def fetch_kairos_data(self, metric_name, site_id, from_date, to_date):
        final_response = []
        try:
            logger.info("*"*20 + "Started hitting kairos" + "*"*20)
            metric_json = [
                {
                    "name": metric_name,
                    "tags": {
                        'site_id': site_id
                    },
                    "group_by": [
                        {
                            "name": "tag",
                            "tags": [
                                "monitoring_id",
                                "parameter_id"
                            ]
                        }
                    ]
                }
            ]
            response = self.kairos_db_obj.fetch_avg_data(metric_json, from_date, to_date)
            if response:
                for each in response["queries"][0]["results"]:
                    temp = {"site_name": each["tags"]["site_name"][0], "parameter_id": each["tags"]["parameter_id"][0],
                            "monitoring_id": each["tags"]["monitoring_id"][0], "values": each["values"]}
                    final_response.append(temp)
        except Exception as e:
            logger.exception(f"Error occurred while fetching kairos data: {str(e)}")
            traceback.print_exc()
        return final_response


def get_color(value):
    if 0 < value <= 50:
        return "scale-one"
    elif 50 < value <= 100:
        return "scale-two"
    elif 100 < value <= 200:
        return "scale-three"
    elif 200 < value <= 300:
        return "scale-four"
    elif 300 < value <= 400:
        return "scale-five"
    else:
        return "scale-six"


def get_remark(value):
    if 0 < value <= 50:
        return "Good"
    elif 50 < value <= 100:
        return "Satisfactory"
    elif 100 < value <= 200:
        return "Moderate"
    elif 200 < value <= 300:
        return "Poor"
    elif 300 < value <= 400:
        return "Very Poor"
    else:
        return "Severe"
