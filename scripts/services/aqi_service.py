# _______Start of import Statements _____________
import base64
import json
from flask import request, Blueprint, send_file
import os
from scripts.constants.app_configuration import report
from scripts.core.handlers.aqi_handler import AqiHandler
from scripts.logging.glens_logging import logger
from scripts.constants import app_constants
from scripts.core.handlers.avg_aqi_handler import AvgAqiHandler

# _______End of the Import Statements ___________

aqi_service = Blueprint("aqi_service", __name__)
avg_aqi_obj = AvgAqiHandler()
aqi_obj = AqiHandler()


@aqi_service.route(app_constants.fetch_historical_aqi, methods=[app_constants.POST])
def fetch_industries_list():
    """

    This function fetches the industry list from the Mongo DataBase and gives it for main page display
    :return: returns the main page industry list json
    """
    if request.method == app_constants.POST:
        try:
            input_json = json.loads(base64.b64decode(request.get_data()))
            returning_json = aqi_obj.aqi_handler(input_json)
            return base64.b64encode((json.dumps(returning_json)).encode())
        except Exception as e:
            logger.error("Error while fetching industry list : " + str(e))
            data = {"status": False, "message": "method not support"}
            return base64.b64encode((json.dumps(data)).encode())


@aqi_service.route(app_constants.fetch_avg_aqi, methods=[app_constants.POST])
def fetch_avg_aqi_industries_list():
    """

    This function fetches the industry list from the Mongo DataBase and gives it for main page display
    :return: returns the main page industry list json
    """
    if request.method == app_constants.POST:
        try:
            input_json = json.loads(base64.b64decode(request.get_data()))
            returning_json = avg_aqi_obj.avg_aqi_handler(input_json)
            print("returning json:", returning_json)
            return base64.b64encode((json.dumps(returning_json)).encode())
        except Exception as e:
            logger.error("Error while fetching industry list : " + str(e))
            data = {"status": False, "message": "method not support"}
            return base64.b64encode((json.dumps(data)).encode())


@aqi_service.route(app_constants.download, methods=[app_constants.GET])
def download():
    if request.method == app_constants.GET:
        try:
            file_name = request.get_arguments("name", strip=False)
            try:
                if "/" not in file_name[0] and "/root" not in file_name[0] and\
                        "/root/" not in file_name[0] and "." not in \
                        file_name[0] and "*" not in file_name[0]:
                    if os.path.exists(report + str(file_name[0] + ".xlsx")):
                        os.remove(report + file_name[0] + ".xlsx")
                    if os.path.exists(report + str(file_name[0] + ".csv")):
                        os.remove(report + file_name[0] + ".csv")
                    logger.info("Files Deleted Successfully")
                else:
                    logger.info("Invalid file names. Filename should  not contain any special characters. : " +
                                file_name[0])
            except Exception as es:
                logger.error(str(es))
                return base64.b64encode((json.dumps(str(es))).encode()), {'Content-Type': 'text/plain; charset=utf-8',
                                                                          'Server': "GLens",
                                                                          'X-Content-Type-Options': "nosniff",
                                                                          'Access-Control-Allow-Origin': '*'}

            return send_file(report + file_name[0], as_attachment=True), {
                        'Content-Type': 'application/csv; text/html',
                        'Server': "GLens", 'X-Content-Type-Options': "nosniff",
                        'Access-Control-Allow-Origin': '*', 'Content-Description': 'File Transfer',
                        'Content-Disposition': 'attachment; filename=' + file_name[0]}

        except Exception as es:
            logger.error("Exception occurred while downloading report " + str(es))
            return base64.b64encode((json.dumps(str(es))).encode()), {'Content-Type': 'text/plain; charset=utf-8',
                                                                      'Server': "GLens",
                                                                      'X-Content-Type-Options': "nosniff",
                                                                      'Access-Control-Allow-Origin': '*'}
