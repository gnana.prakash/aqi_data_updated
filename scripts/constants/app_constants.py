from scripts.constants import app_configuration

POST = "POST"
GET = "GET"
fetch_historical_aqi = app_configuration.api_service_url + "/historical_aqi"
fetch_avg_aqi = app_configuration.api_service_url + "/download_city_report"
download = app_configuration.api_service_url + "/download"

city_list = ["Amaravati", "Anantapur", "Chittoor", "Eluru", "Guntur", "Kadapa", "Kakinada", "Kurnool", "Mandadam",
             "Nellore", "Ongole", "Rajahmundry", "Srikakulam", "Tirumala", "Tirupati", "Vijayawada", "Visakhapatnam",
             "Vizianagaram", "Yerraguntla"]
