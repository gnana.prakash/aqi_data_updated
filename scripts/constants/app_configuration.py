from configparser import ConfigParser

parser = ConfigParser()
parser.read("conf/application.conf")

api_service_url = "/glens/appcb/aqi/api/v1.0"

# SERVER
PORT = parser.get("server", 'PORT')
VERSION = parser.get("server", 'VERSION')

# LOG
log_level = parser.get("log", "log_level")
file_name = parser.get("log", "file_name")
basepath = parser.get("log", "basepath")
wait_time = parser.getint("log", "wait_time")


# Service Settings
data_url = parser.get("serviceSetting", "data_url")
report = parser.get("serviceSetting", "report_folder")
